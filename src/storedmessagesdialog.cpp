#include "storedmessagesdialog.h"
#include "ui_storedmessagesdialog.h"

#include <QFileDialog>
#include <QFile>

#include <QDebug>

StoredMessagesDialog::StoredMessagesDialog(
                        const QList<ColorMessage> & countdown_entries,
                        const QStringList & remoteDisplay_entries,
                        QWidget *parent) :
  QDialog(parent),
  ui(new Ui::StoredMessagesDialog)
{
  ui->setupUi(this);
  ui->countdown_entry_listWidget->setSelectionMode(
        QAbstractItemView::ContiguousSelection);
  ui->remoteDisplay_entry_listWidget->setSelectionMode(
        QAbstractItemView::ContiguousSelection);

  countdown_list_ = countdown_entries;
  remoteDisplay_list_ = remoteDisplay_entries;

  for (ColorMessage x : countdown_list_)
  {
    ui->countdown_entry_listWidget->addItem(x.message());
    QListWidgetItem* item = ui->countdown_entry_listWidget->item(ui->countdown_entry_listWidget->count()-1);
    item->setForeground(QBrush(ColorMessage::color_list_[x.color_model_index()][0]));
    item->setBackground(QBrush(ColorMessage::color_list_[x.color_model_index()][1]));
  }
  for (int i=0; i<ui->countdown_entry_listWidget->count();i++)
  {
    QListWidgetItem* item = ui->countdown_entry_listWidget->item(i);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
  }
  ui->remoteDisplay_entry_listWidget->addItems(remoteDisplay_list_);
  for (int i=0; i<ui->remoteDisplay_entry_listWidget->count();i++)
  {
    QListWidgetItem* item = ui->remoteDisplay_entry_listWidget->item(i);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
  }

  QList<QList<QColor>> color_list = ColorMessage::color_list_;
  for (int i=0; i<color_list.size();i++)
  {
    ui->color_model_comboBox->addItem("A");
    ui->color_model_comboBox->setItemData(i,QBrush(color_list[i][0]),Qt::ForegroundRole);
    ui->color_model_comboBox->setItemData(i,QBrush(color_list[i][1]),Qt::BackgroundRole);
  }
}

StoredMessagesDialog::~StoredMessagesDialog()
{
  delete ui;
}

void StoredMessagesDialog::on_import_countdown_file_pushButton_clicked()
{
  QString file_name = QFileDialog::getOpenFileName(this, "Ouvrir fichier",
                                                "",
                                                "Texte (*.txt *.csv)");
  if(file_name!="")
  {
    QFile* file = new QFile(file_name);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
      return;

    while(!file->atEnd())
    {
      QString line = file->readLine();
      if (!ColorMessage::message_list(countdown_list_).contains(line.remove("\n")))
        ui->countdown_entry_listWidget->addItem(line);
    }
  }
}

void StoredMessagesDialog::on_import_remoteDisplay_file_pushButton_clicked()
{
  QString file_name = QFileDialog::getOpenFileName(this, "Ouvrir fichier",
                                                "",
                                                "Texte (*.txt *.csv)");
  if(file_name!="")
  {
    QFile* file = new QFile(file_name);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
      return;

    while(!file->atEnd())
    {
      QString line = file->readLine();
      if (!ColorMessage::message_list(countdown_list_).contains(line.remove("\n")))
        ui->remoteDisplay_entry_listWidget->addItem(line);
    }
  }
}

void StoredMessagesDialog::on_delete_countdown_entry_pushButton_clicked()
{
  for(QListWidgetItem* x : ui->countdown_entry_listWidget->selectedItems())
  {
    int i = ui->countdown_entry_listWidget->row(x);
    ui->countdown_entry_listWidget->takeItem(i);
  }
}

void StoredMessagesDialog::on_new_countdown_entry_pushButton_clicked()
{
  QListWidgetItem* item = new QListWidgetItem("");
  item->setFlags(item->flags() | Qt::ItemIsEditable);
  ui->countdown_entry_listWidget->addItem(item);
  ui->countdown_entry_listWidget->setFocus();
  ui->countdown_entry_listWidget->setCurrentRow(
        ui->countdown_entry_listWidget->count()-1);
  ui->countdown_entry_listWidget->editItem(
        ui->countdown_entry_listWidget->currentItem());
}

void StoredMessagesDialog::on_delete_remoteDisplay_entry_pushButton_clicked()
{
  for(QListWidgetItem* x : ui->remoteDisplay_entry_listWidget->selectedItems())
  {
    int i = ui->remoteDisplay_entry_listWidget->row(x);
    ui->remoteDisplay_entry_listWidget->takeItem(i);
  }
}

void StoredMessagesDialog::on_new_remoteDisplay_entry_pushButton_clicked()
{
  QListWidgetItem* item = new QListWidgetItem("");
  item->setFlags(item->flags() | Qt::ItemIsEditable);
  ui->remoteDisplay_entry_listWidget->addItem(item);
  ui->remoteDisplay_entry_listWidget->setFocus();
  ui->remoteDisplay_entry_listWidget->setCurrentRow(
        ui->remoteDisplay_entry_listWidget->count()-1);
  ui->remoteDisplay_entry_listWidget->editItem(
        ui->remoteDisplay_entry_listWidget->currentItem());
}

void StoredMessagesDialog::on_buttonBox_accepted()
{
  countdown_list_.clear();
  remoteDisplay_list_.clear();
  ui->countdown_entry_listWidget->selectAll();
  for(QListWidgetItem* x : ui->countdown_entry_listWidget->selectedItems())
    countdown_list_.append(
          ColorMessage(x->text(),ColorMessage::colorsToIndex(x->foreground().color(),x->background().color())));
  ui->remoteDisplay_entry_listWidget->selectAll();
  for(QListWidgetItem* x : ui->remoteDisplay_entry_listWidget->selectedItems())
    remoteDisplay_list_.append(x->text());
  emit updateCountdownMessages( countdown_list_ );
  emit updateRemoteDisplayMessages( remoteDisplay_list_ );
}

void StoredMessagesDialog::on_countdown_entry_up_pushButton_clicked()
{
  int index = ui->countdown_entry_listWidget->currentRow();
  if (index > 0)
  {
    QListWidgetItem* item = ui->countdown_entry_listWidget->takeItem(index);
    ui->countdown_entry_listWidget->insertItem(index - 1, item);
    ui->countdown_entry_listWidget->setCurrentRow(index - 1);
  }
}

void StoredMessagesDialog::on_countdown_entry_down_pushButton_clicked()
{
  int index = ui->countdown_entry_listWidget->currentRow();
  if (index < ui->countdown_entry_listWidget->count())
  {
    QListWidgetItem* item = ui->countdown_entry_listWidget->takeItem(index);
    ui->countdown_entry_listWidget->insertItem(index + 1, item);
    ui->countdown_entry_listWidget->setCurrentRow(index + 1);
  }
}

void StoredMessagesDialog::on_remote_display_up_pushButton_clicked()
{
  int index = ui->remoteDisplay_entry_listWidget->currentRow();
  if (index > 0)
  {
    QListWidgetItem* item = ui->remoteDisplay_entry_listWidget->takeItem(index);
    ui->remoteDisplay_entry_listWidget->insertItem(index - 1, item);
    ui->remoteDisplay_entry_listWidget->setCurrentRow(index - 1);
  }
}

void StoredMessagesDialog::on_remote_display_down_pushButton_clicked()
{
  int index = ui->remoteDisplay_entry_listWidget->currentRow();
  if (index < ui->remoteDisplay_entry_listWidget->count())
  {
    QListWidgetItem* item = ui->remoteDisplay_entry_listWidget->takeItem(index);
    ui->remoteDisplay_entry_listWidget->insertItem(index + 1, item);
    ui->remoteDisplay_entry_listWidget->setCurrentRow(index + 1);
  }
}

void StoredMessagesDialog::on_color_model_comboBox_currentIndexChanged(int index)
{
  QList<QList<QColor>> color_list = ColorMessage::color_list_;
  QList<QList<QString>> color_name_list = ColorMessage::color_name_list_;

  QPalette palette = ui->color_model_comboBox->palette();
  palette.setColor(QPalette::ButtonText,color_list[index][0]);
  palette.setColor(QPalette::Button,color_list[index][1]);
  ui->color_model_comboBox->setPalette(palette);
  ui->color_model_comboBox->setStyleSheet("color: "+color_name_list[index][0] +
              "; background-color: "+color_name_list[index][1]);

  QListWidgetItem* item = ui->countdown_entry_listWidget->currentItem();
  if(item!=nullptr)
  {
    item->setForeground(QBrush(color_list[index][0]));
    item->setBackground(QBrush(color_list[index][1]));
  }
}

void StoredMessagesDialog::on_countdown_entry_listWidget_currentItemChanged(
    QListWidgetItem *current, QListWidgetItem *previous)
{
  Q_UNUSED(previous);
  ui->color_model_comboBox->setCurrentIndex(
        ColorMessage::colorsToIndex(current->foreground().color(), current->background().color()));
}

