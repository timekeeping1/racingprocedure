#ifndef COUNTDOWN_H
#define COUNTDOWN_H

#include <QObject>
#include <QGuiApplication>
#include <QTimer>
#include <QTime>
#include <QLabel>

class Countdown : public QObject
{
  Q_OBJECT

public:
  struct Options
  {
    QList<int> enabled_screens;
    int template_index;
    int position_index;
    int size_index;
    int message_font_size;
    int countdown_font_size;
    bool bold;
    bool italic;
    QTime time_countdown_start;
    QTime time_countdown_end;
    QTime countdown_duration;
    QString message;
  };

  static const QString options_str(const Options & o)
  {
    QString str;
    for(int x : o.enabled_screens)
      str += QString::number(x) + ",";
    if (str.lastIndexOf(",")>0)
      str.remove(str.lastIndexOf(","),1);
    str += ";";
    str += QString::number(o.template_index) + ";";
    str += QString::number(o.position_index) + ";";
    str += QString::number(o.size_index) + ";";
    str += QString::number(o.message_font_size) + ";";
    str += QString::number(o.countdown_font_size) + ";";
    str += QString::number(o.bold) + ";";
    str += QString::number(o.italic) + ";";
    str += o.time_countdown_start.toString() + ";";
    str += o.time_countdown_end.toString() + ";";
    str += o.countdown_duration.toString() + ";";
    str += o.message + ";";
    return str;
  }

  enum State {Stopped,Programmed,Displaying};
  Q_ENUM(State)

public:
  explicit Countdown(QObject *parent = nullptr);
  Countdown(const Options &, QObject *parent = nullptr);
  void keyPressEvent(QKeyEvent *event);
  bool eventFilter(QObject *object, QEvent *event);

  const Options & options() const;
  void set_options(const Options &newOptions);
  void set_enabled_screens(const QList<int> &);
  void set_template_index(int);
  void set_position_index(int);
  void set_size_index(int);
  void set_message_font_size(int);
  void set_countdown_font_size(int);
  void set_bold(bool);
  void set_italic(bool);
  void set_time_countdown_start(const QTime &);
  void set_time_countdown_end(const QTime &);
  void set_countdown_duration(const QTime &);
  void set_message(const QString &);
  State state() const;

signals:
  void countdownStopped();
  void programmingCountdownError(const QString &);
  void countdownProgrammed(const QTime &);
  void countdownRunning();
  void countdownReadyToDisplay();

public:
  void programCountdown();
  void programCountdown(const Options & );
  void displayCountdown();
  void displayCountdown(const Options &);
  void addSubToCountdown(const QTime &,bool);
  void stop();
  void initLabels();
  void updateLabels();
  QString widgetText(double ratio = 1.0);

private slots:
  void refreshCountdown();
  void waitForCountdownStart();

private:
  Options options_;
  QList<QLabel*> countdown_label_list_;

  QTimer timer_countdown_programmation_;
  QTimer timer_countdown_running_;
  State state_;

};

#endif // COUNTDOWN_H
