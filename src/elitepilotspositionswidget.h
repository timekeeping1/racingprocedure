#ifndef ELITEPILOTSPOSITIONSWIDGET_H
#define ELITEPILOTSPOSITIONSWIDGET_H

#include "qpushbutton.h"
#include <QWidget>
#include <QTimer>

namespace Ui {
class ElitePilotsPositionsWidget;
}

class ElitePilotsPositionsWidget : public QWidget
{
  Q_OBJECT

public:
  struct PilotPosition {
    uint num;
    uint last_update;
    uint decoder_index;
    uint decoder_channel;

    PilotPosition(uint n=0, uint l_u=0, uint d_i=0, uint d_c=0)
      : num(n), last_update(l_u), decoder_index(d_i), decoder_channel(d_c) {}
    bool operator==(PilotPosition p)
    {
      return ((num==p.num)&&
              (last_update==p.last_update)&&
              (decoder_index==p.decoder_index)&&
              (decoder_channel==p.decoder_channel));
    }
    bool operator!=(PilotPosition p)
    {
      return ((num!=p.num)||
              (last_update!=p.last_update)||
              (decoder_index!=p.decoder_index)||
              (decoder_channel!=p.decoder_channel));
    }
    QString debugStr()
    {
      QString out;
      out =  QString::number(num)+"-";
      out += QString::number(last_update)+"-";
      out += QString::number(decoder_index)+"-";
      out += QString::number(decoder_channel);
      return out;
    }
  };
  enum State {
    Track,
    Pit,
    Idle,
    Unknow
  };

public:
  explicit ElitePilotsPositionsWidget(QWidget *parent = nullptr);
  ~ElitePilotsPositionsWidget();
  void reset();

signals:
  void updatePilotsPositionsView();

private slots:
  void instantPassingsDataReceived(const QString &);
  void raceInformationDataReceived(const QString &);
  void updatePilotsPosition();
  void updatePilotsPositionsViewSlot();


private:
  QPushButton* pilotSticker(uint, uint);

private:
  Ui::ElitePilotsPositionsWidget *ui;

  QTimer timer_refresh_display_;

  std::map<uint,struct PilotPosition> pilot_position_map_;
  QString current_race_;
  QString race_status_;
  uint time_;
  QList<uint> trackList_;
  QList<uint> pitList_;
  QList<uint> idleList_;
};

#endif // ELITEPILOTSPOSITIONSWIDGET_H
