#include "elitebroadcastwidget.h"
#include "ui_elitebroadcastwidget.h"

#include <QPixmap>
#include <QSoundEffect>
#include <QDebug>

EliteBroadcastWidget::EliteBroadcastWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::EliteBroadcastWidget)
{
  ui->setupUi(this);
}

EliteBroadcastWidget::~EliteBroadcastWidget()
{
  delete ui;
}

const QString & EliteBroadcastWidget::race_notification()
{
  return race_notification_;
}

void EliteBroadcastWidget::set_race_notification(const QString & race_notification)
{
  race_notification_ = race_notification;
}

bool EliteBroadcastWidget::race_notification_enabled()
{
  return race_notification_enabled_;
}

void EliteBroadcastWidget::set_race_notification_enabled(bool race_notification_enabled)
{
  race_notification_enabled_= race_notification_enabled;
}

bool EliteBroadcastWidget::race_sound_notification_enabled()
{
  return race_sound_notification_enabled_;
}

void EliteBroadcastWidget::set_race_sound_notification_enabled(bool race_sound_notification_enabled)
{
  race_sound_notification_enabled_ = race_sound_notification_enabled;
}

void EliteBroadcastWidget::reset()
{
  folder_ = "";
  race_ = "";
  elapsed_time_ = QTime(0,0,0,0);
  elapsed_laps_ = 0;
  remaining_time_ = QTime(0,0,0,0);
  remaining_laps_ = 0;
  race_flag_ = "";
  race_type_ = 0;
  frozen_elapsed_time_ = false;
}

void EliteBroadcastWidget::updateWidget()
{
  if (folder_== "")
  {
    reset();
    ui->icon_label->setPixmap(QPixmap(":/images/clock.png").scaledToWidth(200));
    ui->line->hide();
    ui->line_2->hide();
    ui->line_3->hide();
    ui->line_4->hide();
    ui->line_5->hide();
  }
  else
  {
    ui->line->show();
    ui->line_2->show();
    ui->line_3->show();
    ui->line_4->show();
    ui->line_5->hide();
  }
  if (folder_.size()> 15)
    ui->folder_label->setText(folder_.insert(
                                folder_.indexOf(' ',0.5*folder_.size()),'\n'));
  else
    ui->folder_label->setText(folder_);
  if (race_.size()> 15)
    ui->race_label->setText(race_.insert(
                                race_.indexOf(' '),'\n'));
  else
    ui->race_label->setText(race_);

  ui->race_label->setText(race_);
  if (race_flag_ == "READY")
    ui->icon_label->setPixmap(QPixmap(":/images/clock.png").scaledToWidth(120));
  else if (race_flag_ == "GREEN FLAG")
    ui->icon_label->setPixmap(QPixmap(":/images/green_flag.png").scaledToWidth(120));
  else if (race_flag_ == "YELLOW FLAG")
    ui->icon_label->setPixmap(QPixmap(":/images/yellow_flag.png").scaledToWidth(120));
  else if (race_flag_ == "SAFETY CAR")
    ui->icon_label->setPixmap(QPixmap(":/images/yellow_flag.png").scaledToWidth(120));
  else if (race_flag_ == "RED FLAG")
    ui->icon_label->setPixmap(QPixmap(":/images/red_flag.png").scaledToWidth(120));
  else if (race_flag_ == "CHECKERED FLAG")
    ui->icon_label->setPixmap(QPixmap(":/images/checkered_flag.png").scaledToWidth(120));
  else if (race_flag_ == "YELLOW SECTOR")
    ui->icon_label->setPixmap(QPixmap(":/images/yellow_flag.png").scaledToWidth(120));
  else if (race_flag_ == "STOP")
    ui->icon_label->setPixmap(QPixmap(":/images/stop.png").scaledToWidth(120));

  if (race_type_==0)
  {
    ui->race_notification_label->clear();
    ui->race_notification_icon->clear();
    ui->line_3->hide();
    if( (race_flag_ == "STOP") || (race_flag_ == "CHECKERED FLAG"))
      ui->race_progression_label->setText("-");
    else
    {
      QString str = "Temps restant \n";
      if ( (remaining_time_.hour()==0) )
        str += remaining_time_.toString("mm:ss");
      else
        str += remaining_time_.toString("hh:mm:ss");
      ui->race_progression_label->setText(str);
    }
  }
  else if (race_type_==1)
  {
    ui->race_progression_label->setText(QString::number(elapsed_laps_)
                                        + " / " + QString::number(
                                          elapsed_laps_+ remaining_laps_)
                                        +" Tours");
    if(race_notification_enabled_)
    {
      double x = race_notification_.split("/")[0].toInt();
      double y = race_notification_.split("/")[1].toInt();
      if(elapsed_laps_ >= static_cast<int>((x/y)*(remaining_laps_+elapsed_laps_))+1)
      {
        ui->race_notification_label->setText(race_notification_ + " de course dépassé");
        ui->race_notification_icon->setPixmap(QPixmap(":/images/checked.png").scaledToWidth(40));
        ui->line_3->show();
        if(race_notification_raised_ == false)
        {
          race_notification_raised_ = true;
          if(race_sound_notification_enabled_)
          {
              QSoundEffect effect;
              effect.setSource(QUrl::fromLocalFile(":/images/notification.wav"));
              effect.setLoopCount(1);
              effect.setVolume(0.5f);
              effect.play();
              // QSound::play(":/images/notification.wav");
          }

        }
      }
      else
      {
        /*ui->race_notification_label->clear();
        ui->race_notification_icon->clear();
        ui->line_3->hide();
        race_notification_raised_ = false;*/
        ui->race_notification_label->setText(race_notification_ + " de course non atteints");
        ui->race_notification_icon->setPixmap(QPixmap(":/images/unchecked.png").scaledToWidth(40));
        ui->line_3->show();
        race_notification_raised_ = false;
      }
    }
  }
  else
  {
    ui->race_progression_label->clear();
  }
}

void EliteBroadcastWidget::raceInformationDataReceived( const QString & data)
{
  // qDebug() << "raceInformationDataReceived" << data;
  QStringList dataL = data.split(';');
  if (dataL.size()<2)
  {
    reset();
    updateWidget();
    return;
  }
  folder_ = dataL[0];
  race_ = dataL[1];
  elapsed_time_ = QTime(0,0,0,0);
  elapsed_time_ = elapsed_time_.addMSecs(dataL[2].toInt());
  elapsed_laps_ = dataL[3].toInt();
  remaining_time_ = QTime(0,0,0,0);
  remaining_time_ = remaining_time_.addMSecs(dataL[4].toInt());
  remaining_laps_ = dataL[5].toInt();
  race_flag_ = dataL[6];
  //timestamp_elapsed_time_ = dataL[7].toInt();
  //folder_id_ = dataL[8];
  //race_id_ = dataL[9];
  race_type_ = dataL[10].toInt();
  //turn_yellow_sector_ = dataL[11];
  //timestamp_packet_ = dataL[12].toInt();
  frozen_elapsed_time_ = dataL[13].toInt();
  updateWidget();
}

void EliteBroadcastWidget::on_sound_notification_checkBox__stateChanged(int arg1)
{
  if(arg1 == Qt::Checked)
    race_sound_notification_enabled_ = true;
  else
    race_sound_notification_enabled_ = false;
}

