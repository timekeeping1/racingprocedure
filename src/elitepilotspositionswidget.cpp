#include "elitepilotspositionswidget.h"
#include "qpushbutton.h"
#include "ui_elitepilotspositionswidget.h"
#include <QDebug>

#define MAX_ROW 10
#define STICKER_SIZE 36
#define REFRESH_TIME 5000
#define IDLE_THRES_S 180

ElitePilotsPositionsWidget::ElitePilotsPositionsWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ElitePilotsPositionsWidget),
  pilot_position_map_{}
{
  ui->setupUi(this);

  connect(this,SIGNAL(updatePilotsPositionsView()),this,SLOT(updatePilotsPositionsViewSlot()));
  connect(&timer_refresh_display_,SIGNAL(timeout()),this,SLOT(updatePilotsPosition()));

  timer_refresh_display_.start(REFRESH_TIME);
}

ElitePilotsPositionsWidget::~ElitePilotsPositionsWidget()
{
  delete ui;
}

void ElitePilotsPositionsWidget::reset()
{
  pilot_position_map_.clear();
}

void ElitePilotsPositionsWidget::instantPassingsDataReceived(const QString & str)
{
  QStringList str_list = str.split(";");
  if(str_list.length()<4)
    return;
  if (str_list[0].toUInt() != 0)
  {
    PilotPosition pilot = PilotPosition(  str_list[0].toUInt(),
                                          str_list[1].toUInt(),
                                          str_list[2].toUInt(),
                                          str_list[3].remove("\r\n").toUInt());
    if (pilot != pilot_position_map_[str_list[0].toUInt()])
    {
      pilot_position_map_[str_list[0].toUInt()] = pilot;
      updatePilotsPosition();
    }
  }
}

void ElitePilotsPositionsWidget::raceInformationDataReceived(const QString & str)
{
  // qDebug() << str;
  QStringList str_list = str.split(";");
  if(str_list.length()<3)
    return;
  time_ = str_list[2].toUInt();
  current_race_ = str_list[1];
  race_status_ = str_list[6];
  if (race_status_ == "STOP")
  { 
    pilot_position_map_.clear();
    trackList_.clear();
    pitList_.clear();
    idleList_.clear();
    updatePilotsPosition();
  }
}

void ElitePilotsPositionsWidget::updatePilotsPosition()
{
  timer_refresh_display_.start(REFRESH_TIME);

  trackList_.clear();
  pitList_.clear();
  idleList_.clear();
  for (auto it=pilot_position_map_.cbegin(); it!=pilot_position_map_.cend(); ++it)
  {
    PilotPosition pilot = it->second;
    if( (pilot.decoder_channel == 1) ||   // STA
        (pilot.decoder_channel == 3) ||   // Inter 1
        (pilot.decoder_channel == 4) ||   // Inter 2
        (pilot.decoder_channel == 5))     //PitOut
    {
      if(static_cast<int>(time_ - pilot.last_update) > (IDLE_THRES_S*1000)) // more than 5min since last update
        idleList_.push_back(pilot.num);
      else
        trackList_.push_back(pilot.num);
    }
    else if( pilot.decoder_channel == 2) // PitIn
    {
      pitList_.push_back(pilot.num);
    }
  }
  std::sort(trackList_.begin(), trackList_.end());
  std::sort(pitList_.begin(), pitList_.end());
  std::sort(idleList_.begin(), idleList_.end());
  ui->trackLabel_->setText("En Piste ("+QString::number(trackList_.size())+")");
  ui->pitLabel_->setText("Stands ("+QString::number(pitList_.size())+")");
  ui->idleLabel_->setText("Inactifs depuis "+QString::number(IDLE_THRES_S/60)+
                          " min ("+QString::number(idleList_.size())+")");
  emit updatePilotsPositionsView();
}

void ElitePilotsPositionsWidget::updatePilotsPositionsViewSlot()
{
  // Clearing Layouts
  while (QLayoutItem* item =  ui->trackGridLayout->takeAt(0))
  {
    Q_ASSERT(! item->layout());
    delete item->widget();
    delete item;
  }
  while (QLayoutItem* item =  ui->pitGridLayout->takeAt(0))
  {
    Q_ASSERT(! item->layout());
    delete item->widget();
    delete item;
  }  while (QLayoutItem* item =  ui->idleGridLayout->takeAt(0))
  {
    Q_ASSERT(! item->layout());
    delete item->widget();
    delete item;
  }

  for (uint i=0; i<trackList_.size(); ++i)
    ui->trackGridLayout->addWidget(pilotSticker(trackList_[i], State::Track),
                                   i/MAX_ROW, i%MAX_ROW);
  for (uint i=0; i<pitList_.size(); ++i)
    ui->pitGridLayout->addWidget(pilotSticker(pitList_[i], State::Pit),
                                 i/MAX_ROW, i%MAX_ROW);
  for (uint i=0; i<idleList_.size(); ++i)
    ui->idleGridLayout->addWidget(pilotSticker(idleList_[i], State::Idle),
                                  i/MAX_ROW, i%MAX_ROW);


  for (uint i=trackList_.size(); i<5*MAX_ROW; i++)
    ui->trackGridLayout->addItem(new QSpacerItem(STICKER_SIZE, STICKER_SIZE,
                                                   QSizePolicy::Fixed, QSizePolicy::Fixed),
                                   i/MAX_ROW, i%MAX_ROW);
  for (uint i=pitList_.size(); i<2*MAX_ROW; i++)
    ui->pitGridLayout->addItem(new QSpacerItem(STICKER_SIZE, STICKER_SIZE,
                                                   QSizePolicy::Fixed, QSizePolicy::Fixed),
                                   i/MAX_ROW, i%MAX_ROW);
  for (uint i=idleList_.size(); i<2*MAX_ROW; i++)
    ui->idleGridLayout->addItem(new QSpacerItem(STICKER_SIZE, STICKER_SIZE,
                                                   QSizePolicy::Fixed, QSizePolicy::Fixed),
                                   i/MAX_ROW, i%MAX_ROW);
}

QPushButton* ElitePilotsPositionsWidget::pilotSticker(uint n, uint state=0)
{
  QPushButton* b = new QPushButton(QString::number(n));
  b->setFixedSize(STICKER_SIZE, STICKER_SIZE);

  QRect rect(QPoint(), b->size());
  rect.adjust(2, 2, -2, -2);
  QRegion region(rect, QRegion::Ellipse);
  b->setMask(region);
  QPalette palette = b->palette();
  switch (state)
  {
  case State::Track:
    palette.setColor(QPalette::Button, QColor("lime"));
    break;
  case State::Pit:
    palette.setColor(QPalette::Button, QColor("blue"));
    break;
  case State::Idle:
    palette.setColor(QPalette::Button, QColor("orange"));
    break;
  default:
    palette.setColor(QPalette::Button, QColor("white"));
    break;
  }

  ;
  b->setPalette(palette);

  return b;
}
