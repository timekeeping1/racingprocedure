#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGuiApplication>
#include <QStandardPaths>
#include <QSettings>
#include <QAction>
#include <QKeyEvent>
#include <QMessageBox>
#include <QScreen>
#include <QLabel>
#include <QTextEdit>
#include <QCheckBox>
#include <QTimer>
#include <QTime>
#include <QDebug>

#include "colormessage.h"
#include "network.h"
#include "countdown.h"
#include "message.h"
#include "elitebroadcastwidget.h"
#include "elitepilotspositionswidget.h"
#include "remotedisplaymessagewidget.h"
#include "storedmessagesdialog.h"

#define MAX_SCREENS 6

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    virtual void keyPressEvent(QKeyEvent *event);
    void closeEvent ( QCloseEvent * event );
    bool eventFilter(QObject *object, QEvent *event);

private:
    void initVariables();
    void initUI();

private slots:
    void EnableMainSettings();
    void disableMainSettings();
    void updateTime();
    void screenCountChanged(QScreen*);
    void changeEnabledScreens();
    void updateEnabledScreensWidget(int, QList<int> = {0});
    void changeModel();
    void setCountdownPreviewStyle();
    void setMessagePreviewStyle();

    void on_time_start_countdown_timeEdit_userTimeChanged(const QTime &);
    void on_time_end_countdown_timeEdit_userTimeChanged(const QTime &);
    void on_duration_lineEdit_timeChanged(const QTime &);

    void stopDisplays();
    void displayCountdown();
    void displayCountdown(Countdown::Options);
    void programCountdown();

    void identifyScreens();
    void stopIdentifyScreens();

    void addSubToCountdown(QTime, bool);
    void addSubToCountdown();

    void displayFullScreenMessage();
    void displayPopUpMessage();
    void displayMessage(Message::Options);

    void on_actionReseau_triggered();
    void on_actionMessages_enregistres_triggered();
    void updateCountdownUI(const QTime &);
    void updateCountdownUI();
    void updateCountdownMessages(const QList<ColorMessage> &);
    void updateNetworkUI();
    void sendTcpUpdateEnabledScreens();
    void updateNetworkCommandLabel(const QString &, const QString &);
    void displayWarningBox(const QString &);

    void on_enabled_all_screens_pushButton__clicked();

signals:
    void addSubToCountdownSignal(QTime, bool);
    void updateNetwork();

private:
    Ui::MainWindow *ui_;
    QMenu *menu_;
    QAction *act_;

    QTime add_sub_;
    QList<ColorMessage> countdown_message_list_;
    QStringList network_command_history_;
    QTimer timer_time_;
    QTimer timer_elite_;

    // QList<QCheckBox*> enabled_screens_checkBox_list_;
    QList<QPointer<QCheckBox> > enabled_screens_checkBox_list_;
    QList<QLabel*> screen_id_;

    QLabel* status_icon_;
    QLabel* status_label_;
    QLabel* clients_label_;
    QPushButton* reload_client_socket_pushbutton_;
    QLabel* elite_connexion_label_;
    QPushButton* reload_elite_connexion_pushbutton_;

    Network network_;
    Message message_widget_;
    Countdown countdown_widget_;
    EliteBroadcastWidget elite_broadcast_widget_;
    ElitePilotsPositionsWidget elite_pilots_positions_widget_;

    RemoteDisplayMessageWidget remote_display_widget_;

    QSettings settings_;

};

#endif // MAINWINDOW_H
