#include "network.h"

Network::Network(QObject *parent)
  : QObject(parent),
    tcp_server_(new QTcpServer()),
    tcp_client_socket_(new QTcpSocket()),
    elite_tcp_client_socket_(new QTcpSocket()),
    remoteDisplay_tcp_client_socket_(new QTcpSocket())
{
  connect(tcp_server_,  SIGNAL(newConnection()), this, SLOT(newConnectionSlot()));
  tcp_client_socket_->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
  elite_tcp_client_socket_->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
  remoteDisplay_tcp_client_socket_->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
}

const Network::Mode &Network::network_option() const
{
  return network_option_;
}

QString Network::network_option_str() const
{
  QString str;
  if (network_option_ == Mode::Local)
    str = "local";
  else if (network_option_ == Mode::Server)
    str = "server";
  else if (network_option_ == Mode::Client)
    str = "client";
  else
    str = "local";
  return str;
}

void Network::set_network_option(const Mode &network_option)
{
  network_option_ = network_option;
}

const QString &Network::server_ip() const
{
  return server_ip_;
}

void Network::set_server_ip(const QString &new_server_ip)
{
  server_ip_ = new_server_ip;
}

int Network::server_port() const
{
  return server_port_;
}

void Network::set_server_port(int new_server_port)
{
  server_port_ = new_server_port;
}

int Network::client_port() const
{
  return client_port_;
}

void Network::set_client_port(int new_client_port)
{
  client_port_ = new_client_port;
}

bool Network::connect_to_elite() const
{
  return connect_to_elite_;
}

void Network::set_connect_to_elite(bool new_connect_to_elite)
{
  connect_to_elite_ = new_connect_to_elite;
}

const QString &Network::elite_master_ip() const
{
  return elite_master_ip_;
}

void Network::set_elite_master_ip(const QString &new_elite_master_ip)
{
  elite_master_ip_ = new_elite_master_ip;
}

int Network::elite_master_port() const
{
  return elite_master_port_;
}

void Network::set_elite_master_port(int new_elite_master_port)
{
  elite_master_port_ = new_elite_master_port;
}

bool Network::serverIsListening() const
{
  return this->tcp_server_->isListening();
}

int Network::serverClientsConnected() const
{
  return this->tcp_server_sockets_.size();
}

QAbstractSocket::SocketState Network::clientSocketState() const
{
  return this->tcp_client_socket_->state();
}

QAbstractSocket::SocketState Network::eliteSocketState() const
{
  return this->elite_tcp_client_socket_->state();
}

QAbstractSocket::SocketState Network::remoteDisplaySocketState() const
{
  return this->remoteDisplay_tcp_client_socket_->state();
}


void Network::updateNetwork()
{
  tcp_client_socket_->disconnect();
  tcp_client_socket_->disconnectFromHost();
  tcp_client_socket_->close();

  buffer_.clear();

  for(QTcpSocket* socket : tcp_server_sockets_)
  {
    socket->disconnect();
    socket->disconnectFromHost();
    socket->deleteLater();
  }
  tcp_server_sockets_.clear();
  tcp_server_->close();

  if(network_option() == Mode::Server)
  {
    tcp_server_->listen(QHostAddress::Any,static_cast<quint16>(server_port_));
  }
  else if(network_option() == Mode::Client)
  {
    tcp_client_socket_->connectToHost(server_ip_,static_cast<quint16>(client_port_));
    tcp_client_socket_->waitForConnected(1000);
    if(tcp_client_socket_->state() == QAbstractSocket::ConnectedState)
    {
      connect(tcp_client_socket_,SIGNAL(disconnected()),this,SLOT(updateNetwork()));
      connect(tcp_client_socket_,SIGNAL(readyRead()),this,SLOT(clientDataReceived()));
    }
  }

  emit updateNetworkUI();
}

void Network::updateEliteNetwork()
{
  qDebug() << "elite redemarré";
  elite_tcp_client_socket_->disconnect();
  elite_tcp_client_socket_->close();
  remoteDisplay_tcp_client_socket_->disconnect();
  remoteDisplay_tcp_client_socket_->close();

  elite_buffer_.clear();

  if(connect_to_elite_)
  {
    elite_tcp_client_socket_->connectToHost(
          elite_master_ip_,static_cast<quint16>(elite_master_port_));
    remoteDisplay_tcp_client_socket_->connectToHost(
          elite_master_ip_,static_cast<quint16>(elite_master_port_));
    if((elite_tcp_client_socket_->state() == QAbstractSocket::ConnectedState) ||
       (elite_tcp_client_socket_->state() == QAbstractSocket::ConnectingState))
    {
      elite_tcp_client_socket_->write("$3\r\n");
      elite_tcp_client_socket_->waitForConnected(1000);
      connect(elite_tcp_client_socket_,SIGNAL(readyRead()),this,SLOT(eliteDataReceived()));
      connect(elite_tcp_client_socket_,SIGNAL(disconnected()),this,SLOT(updateEliteNetwork()));
    }
    if((remoteDisplay_tcp_client_socket_->state() == QAbstractSocket::ConnectedState) ||
       (remoteDisplay_tcp_client_socket_->state() == QAbstractSocket::ConnectingState))
    {
      remoteDisplay_tcp_client_socket_->write("$4\r\n");
      remoteDisplay_tcp_client_socket_->waitForConnected(1000);
      connect(remoteDisplay_tcp_client_socket_,SIGNAL(disconnected()),this,SLOT(updateEliteNetwork()));
    }
  }
  emit updateNetworkUI();
}

void Network::newConnectionSlot()
{
  while(tcp_server_->hasPendingConnections())
  {
    tcp_server_sockets_.append(tcp_server_->nextPendingConnection());
    connect(tcp_server_sockets_.last(),SIGNAL(disconnected()), this,SLOT(removeSocketFromTcpServerSockets()));
    connect(tcp_server_sockets_.last(),SIGNAL(readyRead()),    this,SLOT(serverDataReceived()));
    emit updateEnabledScreens();
    emit updateInformation(tcp_server_sockets_.last()->peerAddress().toString().remove(0,7),
                           "Connexion");
  }
  emit updateNetworkUI();
}

void Network::removeSocketFromTcpServerSockets()
{
  QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
  socket->close();
  socket->deleteLater();
  tcp_server_sockets_.removeOne(socket);
  emit updateNetworkUI();
}

// %0 : Commande qui spécifie les écrans à afficher
void Network::clientDataReceived()
{
  QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
  QByteArray data = socket->readAll();
  buffer_.append(data);
  while((buffer_[0] != '%') && (buffer_.size()>0))
    buffer_.remove(0,1);
  if(buffer_.contains("end"))
  {
    buffer_.remove(0,1);
    if(buffer_.startsWith(char(ServerCommand::UpdateEnabledScreens)))
    {
      QList<QByteArray> bufferL =  buffer_.split(';');
      QList<int> enabledScreens;
      int screens = bufferL[1].toInt();
      for(QByteArray x : bufferL[2].split(','))
          enabledScreens.append(x.toInt());
      QList<int> tmp;
      tmp.append(0);
      tmp.append(1);
      tmp.append(2);
      tmp.append(3);
      tmp.append(4);
      tmp.append(5);
      emit updateEnabledScreens(screens,tmp);
    }
    if(buffer_.startsWith(char(ServerCommand::Stop)))
    {
      emit resetUI();
    }
    buffer_.clear();
  }
}

// $0 : commandes d'identification des écrans
// $1 : commandes d'arrêt d'identification des écrans
// $2 : commandes d'envoi d'un décompte. arguments :
//    écran sur lesquels afficher
//    index modèle de couleurs
//    index position
//    index taille
//    taille police du message
//    taille police du décompte
//    textes en gras
//    textes en italic
//    durée du décompte
//    message
// $3 : commande d'ajout/retrait de temps au décompte
// $4 : commande de STOP d'affichage
// $5 : commande d'affichage message plein écran ou Popup
void Network::serverDataReceived()
{
  QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
  QByteArray data = socket->readAll();
  buffer_.append(data);
  while((buffer_[0] != '$') && (buffer_.size()>0)) buffer_.remove(0,1);
  if(buffer_.contains("end"))
  {
    buffer_.remove(0,1);
    if(buffer_.startsWith(char(ClientCommand::IdentifyScreens)))
    {
      emit identifyScreens();
    }
    else if(buffer_.startsWith(char(ClientCommand::StopIdentifyScreens)))
    {
      emit stopIdentifyScreens();
    }
    else if(buffer_.startsWith(char(ClientCommand::DisplayCountdown)))
    {
      Countdown::Options countdown_options;
      QList<QByteArray> bufferL =  buffer_.split(';');
      QList<int> enabledScreens;
      for(QByteArray x : bufferL[1].split(','))
        enabledScreens.append(x.toInt());
      countdown_options.enabled_screens = enabledScreens;
      countdown_options.template_index = bufferL[2].toInt();
      countdown_options.position_index = bufferL[3].toInt();
      countdown_options.size_index = bufferL[4].toInt();
      countdown_options.message_font_size = bufferL[5].toInt();
      countdown_options.countdown_font_size = bufferL[6].toInt();
      countdown_options.bold = bufferL[7].toInt();
      countdown_options.italic = bufferL[8].toInt();
      countdown_options.time_countdown_start = QTime::fromString(QString(bufferL[9]));
      countdown_options.time_countdown_end = QTime::fromString(QString(bufferL[10]));
      countdown_options.countdown_duration = QTime::fromString(QString(bufferL[11]));
      countdown_options.message = bufferL[12];

      emit displayCountdown(countdown_options);
      QString str;
      str = "Décompte lancé pour " + countdown_options.countdown_duration.toString();

      emit updateInformation(socket->peerAddress().toString().remove(0,7),
                             str);
    }
    else if(buffer_.startsWith(char(ClientCommand::ProgramCountdown)))
    {
      Countdown::Options countdown_options;
      QList<QByteArray> bufferL =  buffer_.split(';');
      QList<int> enabledScreens;
      for(QByteArray x : bufferL[1].split(','))
        enabledScreens.append(x.toInt());
      countdown_options.enabled_screens = enabledScreens;
      countdown_options.template_index = bufferL[2].toInt();
      countdown_options.position_index = bufferL[3].toInt();
      countdown_options.size_index = bufferL[4].toInt();
      countdown_options.message_font_size = bufferL[5].toInt();
      countdown_options.countdown_font_size = bufferL[6].toInt();
      countdown_options.bold = bufferL[7].toInt();
      countdown_options.italic = bufferL[8].toInt();
      countdown_options.time_countdown_start = QTime::fromString(QString(bufferL[9]));
      countdown_options.time_countdown_end = QTime::fromString(QString(bufferL[10]));
      countdown_options.countdown_duration = QTime::fromString(QString(bufferL[11]));
      countdown_options.message = bufferL[12];

      QString str;
      str = "Décompte programmé à " + countdown_options.time_countdown_start.toString()
            + " pour " + countdown_options.countdown_duration.toString();

      emit updateInformation(socket->peerAddress().toString().remove(0,7),
                             str);
    }
    else if(buffer_.startsWith(char(ClientCommand::AddSubToCountdown)))
    {
      QList<QByteArray> bufferL =  buffer_.split(';');
      QTime delta = QTime::fromString(QString(bufferL[1]));
      bool add_sub = bufferL[2].toInt();
      emit addSubToCountdown(delta,add_sub);

      QString str;
      if(add_sub)
        str = "ajout de ";
      else
        str = "retrait de ";
      str += delta.toString() + " au décompte";
      emit updateInformation(socket->peerAddress().toString().remove(0,7),
                             str);
    }
    else if(buffer_.startsWith(char(ClientCommand::Stop)))
    {
      emit stopDisplaying();

      emit updateInformation(socket->peerAddress().toString().remove(0,7),
                              "Décompte stoppé" );
    }
    else if(buffer_.startsWith(char(ClientCommand::DisplayMessage)))
    {
      Message::Options message_options;
      QList<QByteArray> bufferL =  buffer_.split(';');
      QList<int> enabled_screens;
      for(QByteArray x : bufferL[1].split(','))
        enabled_screens.append(x.toInt());
      message_options.enabled_screens = enabled_screens;
      message_options.template_index = bufferL[2].toInt();
      message_options.full_screen = bufferL[3].toInt();
      message_options.message_font_size = bufferL[4].toInt();
      message_options.bold = bufferL[5].toInt();
      message_options.italic = bufferL[6].toInt();
      message_options.message = bufferL[7];
      emit displayMessage(message_options);
    }
    buffer_.clear();
  }
}

void Network::eliteDataReceived()
{
  QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
  QByteArray data = socket->readAll();
  elite_buffer_.append(data);
  QRegularExpression re("\\*\\*\\* (?<data_type>\\w+ ?\\w+?) BEGIN \\*\\*\\*(?<n_data> \\(\\d+\\))?\r\n(?<data>[^*]*)\\*\\*\\* (?<data_type_bis>\\w+ ?\\w+?) END \\*\\*\\*\r\n");
  re.setPatternOptions(QRegularExpression::MultilineOption);
  QRegularExpressionMatchIterator it = re.globalMatch(elite_buffer_);
  while (it.hasNext())
  {
    QRegularExpressionMatch match = it.next();

    //if(match.captured("data_type") == "INSTANT PASSING")
      //qDebug() << "NETWORK " << match.captured("data_type") << " " << match.captured("data");

    if(match.captured("data_type") == "RACE INFORMATION")
      emit raceInformationDataReceived(match.captured("data"));
    if(match.captured("data_type") == "RESULTS")
      emit resultsDataReceived(match.captured("data"));
    if(match.captured("data_type") == "PASSINGS")
      emit passingsDataReceived(match.captured("data"));
    if(match.captured("data_type") == "INSTANT PASSING")
      emit instantPassingsDataReceived(match.captured("data"));
    if(match.captured("data_type") == "PASSING CHANGED")
      emit passingChangedDataReceived(match.captured("data"));
    if(match.captured("data_type") == "MESSAGE")
      emit messageDataReceived(match.captured("data"));
    if(match.captured("data_type") == "FLAG CHANGED")
      emit flagChangedDataReceived(match.captured("data"));
    if(match.captured("data_type") == "FOLDER INFORMATIONS")
      emit folderInformationDataReceived(match.captured("data").split("\r\n"));
    if(match.captured("data_type") == "FIELD LANGUAGE")
      emit fieldLanguageDataReceived(match.captured("data").split(" \r\n"));
    if(match.captured("data_type") == "FLAG DATABASE")
      emit flagDatabaseDataReceived(match.captured("data"));
    if(match.captured("data_type") == "PASSINGS DATABASE")
      emit passingsDatabaseDataReceived(match.captured("data"));

    elite_buffer_.remove(match.captured(0));
    // qDebug() << "removed " << match.captured(0) << " from buffer";
    // qDebug() << "Buffer Size : " << elite_buffer_.size();
  }
}

void  Network::getEliteClassification()
{
  if(elite_tcp_client_socket_->state() == QAbstractSocket::ConnectedState)
  {
    elite_tcp_client_socket_->write("$7\r\n");
  }
}

void Network::clientCommandIdentifyScreens()
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::IdentifyScreens));
  dataToSend.append(";");
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::clientCommandStopIdentifyScreens()
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::StopIdentifyScreens));
  dataToSend.append(";");
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::clientCommandDisplayCountdown(const Countdown::Options & co)
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::DisplayCountdown));
  dataToSend.append(";");
  dataToSend.append(Countdown::options_str(co).toUtf8());
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::clientCommandProgramCountdown(const Countdown::Options & co)
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::ProgramCountdown));
  dataToSend.append(";");
  dataToSend.append(Countdown::options_str(co).toUtf8());
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::clientCommandAddSubToCountdown(QTime t,bool b)
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::AddSubToCountdown));
  dataToSend.append(";");
  dataToSend.append(t.toString().toUtf8());
  dataToSend.append(";");
  dataToSend.append(QString::number(b).toUtf8());
  dataToSend.append(";");
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::clientCommandStop()
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::Stop));
  dataToSend.append(";");
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::clientCommandDisplayMessage(Message::Options mo)
{
  QByteArray dataToSend;
  dataToSend.append("$");
  dataToSend.append(char(ClientCommand::DisplayMessage));
  dataToSend.append(";");
  dataToSend.append(Message::options_str(mo).toUtf8());
  dataToSend.append("end");
  tcp_client_socket_->write(dataToSend);
}

void Network::serverCommandUpdateEnabledScreens(int n,QList<int> l)
{
  QByteArray dataToSend;
  dataToSend.append("%");
  dataToSend.append(char(ServerCommand::UpdateEnabledScreens));
  dataToSend.append(";");
  dataToSend.append(QString::number(n).toUtf8());
  dataToSend.append(";");
  QString str;
  for(int x : l)
    str += QString::number(x) + ",";
  if (str.lastIndexOf(",")>0)
    str.remove(str.lastIndexOf(","),1);
  str += ";";
  dataToSend.append("end");

  for(QAbstractSocket* socket : tcp_server_sockets_)
  {
    if(socket->state() == QAbstractSocket::ConnectedState)
     {
      socket->write(dataToSend);
    }
  }
}

void Network::serverCommandStop()
{
  QByteArray dataToSend;
  dataToSend.append("%");
  dataToSend.append(char(ServerCommand::Stop));
  dataToSend.append(";");
  dataToSend.append("end");

  for(QAbstractSocket* socket : tcp_server_sockets_)
  {
    if(socket->state() == QAbstractSocket::ConnectedState)
     {
      socket->write(dataToSend);
    }
  }
}

void Network::sendRemoteDisplayMessage(const QString & str)
{
  QByteArray data = str.toUtf8();
  if(remoteDisplay_tcp_client_socket_->state() == QAbstractSocket::ConnectedState)
    remoteDisplay_tcp_client_socket_->write(data);
}

void Network::clearBuffers()
{
  buffer_.clear();
  elite_buffer_.clear();
  elite_tcp_client_socket_->flush();
}
