#include "colormessage.h"
#include <QDebug>

QList<QList<QColor>> ColorMessage::color_list_ =
      { QList<QColor>({Qt::black,Qt::white}),
        QList<QColor>({Qt::white,Qt::black}),
        QList<QColor>({Qt::green,Qt::black}),
        QList<QColor>({Qt::green,Qt::white}),
        QList<QColor>({Qt::gray,Qt::white}),
        QList<QColor>({Qt::black,Qt::gray}),
        QList<QColor>({Qt::red,Qt::black}),
        QList<QColor>({Qt::red,Qt::white})};

QList<QList<QString>> ColorMessage::color_name_list_ =
      { QList<QString>({"black","white"}),
        QList<QString>({"white","black"}),
        QList<QString>({"green","black"}),
        QList<QString>({"green","white"}),
        QList<QString>({"gray","white"}),
        QList<QString>({"black","gray"}),
        QList<QString>({"red","black"}),
        QList<QString>({"red","white"})};

ColorMessage::ColorMessage(const QString& message, const int index)
{
  message_ = message;
  color_model_index_ = index;
}

const QString ColorMessage::message()
{
  return message_;
}

void ColorMessage::set_message(const QString& message)
{
  message_ = message;
}

int ColorMessage::color_model_index()
{
  return color_model_index_;
}

void ColorMessage::set_color_model_index(const int index)
{
  color_model_index_ = index;
}

const QColor ColorMessage::background_color()
{
  return color_list_[color_model_index_][1];
}

const QColor ColorMessage::font_color()
{
  return color_list_[color_model_index_][0];
}

const QString ColorMessage::background_color_name()
{
  return color_name_list_[color_model_index_][1];
}

const QString ColorMessage::font_color_name()
{
  return color_name_list_[color_model_index_][0];
}

const QString ColorMessage::toSettings()
{
  return QString(this->message()+"\\"+QString::number(this->color_model_index()));
}

ColorMessage ColorMessage::fromSettings(const QString& setting)
{
  QString message = setting.split("\\")[0];
  if(setting.split("\\").count() < 2)
    return ColorMessage(message, 0);
  int index = setting.split("\\")[1].toInt();
  return ColorMessage(message, index);
}

QList<ColorMessage> ColorMessage::listFromSettings(const QStringList& settings)
{
  QList<ColorMessage> list;
  for (QString x : settings)
  {
    list.append(ColorMessage::fromSettings(x));
  }

  return list;
}

QString ColorMessage::toSettings( ColorMessage& color_message)
{
  return QString(color_message.message()+"\\"+QString::number(color_message.color_model_index()));
}

QStringList ColorMessage::toSettings( QList<ColorMessage>& color_message_list)
{
  QStringList list;
  for (ColorMessage x : color_message_list)
  {
    list.append(x.toSettings());
  }
  return list;
}

QStringList ColorMessage::message_list( QList<ColorMessage>& color_message_list)
{
  QStringList list;
  for (ColorMessage x : color_message_list)
    list.append(x.message());
  return list;
}

int ColorMessage::colorsToIndex(const QColor& text_color, const QColor& background_color)
{
  int index=0;
  if( text_color == Qt::black)
  {
    if( background_color == Qt::white)
      index =0;
    else if( background_color == Qt::gray)
      index =5;
  }
  else if( text_color == Qt::white)
  {
    index =1;
  }
  else if( text_color == Qt::green)
  {
    if( background_color == Qt::black)
      index =2;
    else if( background_color == Qt::white)
      index =3;
  }
  else if( text_color == Qt::gray)
  {
    index =4;
  }
  else if( text_color == Qt::red)
  {
    if( background_color == Qt::black)
      index =6;
    else if( background_color == Qt::white)
      index =7;
  }
  return index;
}
