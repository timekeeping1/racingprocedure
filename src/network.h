#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QtNetwork>

#include "countdown.h"
#include "message.h"

class Network : public QObject
{
    Q_OBJECT

public:
  enum class Mode :           char {  Local, Client, Server };
  enum class ServerCommand :  char {  UpdateEnabledScreens,
                                      Stop };
  enum class ClientCommand :  char {  IdentifyScreens,
                                      StopIdentifyScreens,
                                      DisplayCountdown,
                                      ProgramCountdown,
                                      AddSubToCountdown,
                                      Stop,
                                      DisplayMessage };

  Q_ENUM(Mode)
  Q_ENUM(ServerCommand)
  Q_ENUM(ClientCommand)

public:
    explicit Network(QObject *parent = nullptr);

  // accessor
    const Mode &network_option() const;
    QString network_option_str() const;
    void set_network_option(const Mode &);
    const QString &server_ip() const;
    void set_server_ip(const QString &);
    int server_port() const;
    void set_server_port(int);
    int client_port() const;
    void set_client_port(int);
    bool connect_to_elite() const;
    void set_connect_to_elite(bool);
    const QString &elite_master_ip() const;
    void set_elite_master_ip(const QString &);
    int elite_master_port() const;
    void set_elite_master_port(int);

    bool serverIsListening() const;
    int serverClientsConnected() const;
    QAbstractSocket::SocketState clientSocketState() const;
    QAbstractSocket::SocketState eliteSocketState() const;
    QAbstractSocket::SocketState remoteDisplaySocketState() const;

signals:
    void updateNetworkUI();
    void updateEnabledScreens(int,QList<int>);
    void identifyScreens();
    void stopIdentifyScreens();
    void addSubToCountdown(QTime,bool);
    void stopDisplaying();
    void displayCountdown(Countdown::Options);
    void displayMessage(Message::Options);
    void updateInformation(const QString &, const QString &);
    void resetUI();
    void updateEnabledScreens();

    void raceInformationDataReceived(const QString &);
    void resultsDataReceived(const QString &);
    void passingsDataReceived(const QString &);
    void instantPassingsDataReceived(const QString &);
    void passingChangedDataReceived(const QString &);
    void messageDataReceived(const QString &);
    void flagChangedDataReceived(const QString &);
    void folderInformationDataReceived(const QStringList &);
    void fieldLanguageDataReceived(const QStringList &);
    void flagDatabaseDataReceived(const QString &);
    void passingsDatabaseDataReceived(const QString &);

private slots:
    void updateNetwork();
    void updateEliteNetwork();
    void newConnectionSlot();
    void removeSocketFromTcpServerSockets();
    void clientDataReceived();
    void serverDataReceived();
    void eliteDataReceived();
    void getEliteClassification();
    void sendRemoteDisplayMessage(const QString &);
    void clearBuffers();

public: 
    void clientCommandIdentifyScreens();
    void clientCommandStopIdentifyScreens();
    void clientCommandDisplayCountdown(const Countdown::Options &);
    void clientCommandProgramCountdown(const Countdown::Options &);
    void clientCommandAddSubToCountdown(QTime,bool);
    void clientCommandStop();
    void clientCommandDisplayMessage(Message::Options);
    void serverCommandUpdateEnabledScreens(int,QList<int>);
    void serverCommandStop();
    void serverCommandActualState(Countdown::State, Message::State);

private:
    QTcpServer* tcp_server_;
    QTcpSocket* tcp_client_socket_;
    QList<QTcpSocket*> tcp_server_sockets_;
    QTcpSocket* elite_tcp_client_socket_;
    QTcpSocket* remoteDisplay_tcp_client_socket_;

    QByteArray buffer_;
    QString elite_buffer_;

    Mode network_option_;
    QString server_ip_;
    int server_port_;
    int client_port_;
    bool connect_to_elite_;
    QString elite_master_ip_;
    int elite_master_port_;
    bool race_notification_enabled_;
    QString race_notification_;
};

#endif // NETWORK_H
