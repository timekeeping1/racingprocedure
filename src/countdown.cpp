#include "countdown.h"
#include "mainwindow.h"

#include <QDebug>

Countdown::Countdown(QObject *parent) : QObject(parent)
{
  connect(&timer_countdown_running_,SIGNAL(timeout()),this,SLOT(refreshCountdown()));
  connect(&timer_countdown_programmation_,SIGNAL(timeout()),this,SLOT(waitForCountdownStart()));
  state_ = State::Stopped;
}

void Countdown::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
      case Qt::Key_Enter:
        break;
      case Qt::Key_Return:
        break;
      case Qt::Key_Escape:
        stop();
        break;
      default:
        break;
    }
}

bool Countdown::eventFilter(QObject *object, QEvent *event)
{
    Q_UNUSED(object);
    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
    if (keyEvent->key() == Qt::Key_Escape)
    {
        stop();
        return true;
    } else return false;
}

const Countdown::Options &Countdown::options() const
{
  return options_;
}

void Countdown::set_options(const Options &newOptions)
{
  options_ = newOptions;
}

void Countdown::set_enabled_screens(const QList<int> & enabled_screens)
{
  options_.enabled_screens = enabled_screens;
}

void Countdown::set_template_index(int template_index)
{
  options_.template_index = template_index;
}

void Countdown::set_position_index(int position_index)
{
  options_.position_index = position_index;
}

void Countdown::set_size_index(int size_index)
{
  options_.size_index = size_index;
}

void Countdown::set_message_font_size(int message_font_size)
{
  options_.message_font_size = message_font_size;
}

void Countdown::set_countdown_font_size(int countdown_font_size)
{
  options_.countdown_font_size = countdown_font_size;
}

void Countdown::set_bold(bool bold)
{
  options_.bold = bold;
}

void Countdown::set_italic(bool italic)
{
  options_.italic = italic;
}

void Countdown::set_time_countdown_start(const QTime & countdown_start)
{
  options_.time_countdown_start = countdown_start;
}

void Countdown::set_time_countdown_end(const QTime & countdown_end)
{
  options_.time_countdown_end = countdown_end;
}

void Countdown::set_countdown_duration(const QTime & duration)
{
  options_.countdown_duration = duration;
}

void Countdown::set_message(const QString & message)
{
  options_.message = message;
}

Countdown::State Countdown::state() const
{
  return state_;
}

void Countdown::programCountdown()
{
  Options & co = options_;
  // 3 cas :
  // L'heure de lancement est à venir
  // L'heure de lancement est passée mais pas l'heure de fin
  // Les heures de lancement et de fin sont passées
  //
  // TODO : gérer le passage par minuit (actuellement le programme
  //        refusera la programmation du décompte
  if(QTime::currentTime()<=co.time_countdown_start)
  {
    waitForCountdownStart();
    state_ = State::Programmed;
  }
  else if(QTime::currentTime()<=co.time_countdown_end)
  {

    co.countdown_duration = QTime(0,0,0,0);
    co.countdown_duration = co.countdown_duration.addSecs(
          QTime::currentTime().secsTo(co.time_countdown_end));
    //displayCountdown();
    emit countdownReadyToDisplay();
  }
  else
  {
    stop();
    emit programmingCountdownError("Horaire incorrect");
    emit countdownStopped();
  }
}

void Countdown::programCountdown(const Countdown::Options & Options)
{
  set_options(Options);
  programCountdown();
}

void Countdown::displayCountdown()
{
  state_ = State::Displaying;
  timer_countdown_running_.start(100);
  options_.time_countdown_start = QTime::currentTime();
  options_.time_countdown_end = QTime::currentTime().addSecs(
        60*options_.countdown_duration.minute() + options_.countdown_duration.second());
  initLabels();
}

void Countdown::displayCountdown(const Countdown::Options & Options)
{
  set_options(Options);
  displayCountdown();
}

void Countdown::addSubToCountdown(const QTime & delta,bool add)
{
  if (add)
    options_.time_countdown_end = options_.time_countdown_end.addSecs(
          60*delta.minute() + delta.second());
  else
    options_.time_countdown_end = options_.time_countdown_end.addSecs(
          -60*delta.minute() - delta.second());
}

void Countdown::refreshCountdown()
{
  if(options_.countdown_duration == QTime(0,0,0,0))
  {
    stop();
    timer_countdown_running_.stop();
    emit countdownStopped();
    return;
  }
  options_.countdown_duration = QTime(0,0,0,0);
  options_.countdown_duration = options_.countdown_duration.addSecs(
        QTime::currentTime().secsTo(options_.time_countdown_end));
  timer_countdown_running_.start(100);
  updateLabels();
  emit countdownRunning();
}

void Countdown::waitForCountdownStart()
{
  Options & co = options_;
  QTime time_remaining_before_start(0,0,0,0);
  time_remaining_before_start = time_remaining_before_start.addSecs(
        QTime::currentTime().secsTo(co.time_countdown_start));
  emit countdownProgrammed(time_remaining_before_start);
  if(time_remaining_before_start > QTime(0,0,0,0))
  {
    timer_countdown_programmation_.start(100);
  }
  else
  {
    timer_countdown_programmation_.stop();
    emit countdownReadyToDisplay();
  }
}


void Countdown::initLabels()
{
  for(int i=0; i<qApp->screens().size();i++)
  {
    QScreen* screen = qApp->screens()[i];
    int screen_height = screen->size().height();
    int screen_length = screen->size().width();
    int widget_height;
    int widget_length;
    int x0 = screen->geometry().left();
    int y0 = screen->geometry().top();
    int widget_x0;
    int widget_y0;

    switch(options_.size_index)
    {
      case 0:
        widget_height = (screen_height/10);
        widget_length = (3*screen_length/4);
        widget_x0 = (x0 + 1*screen_length/8);
        break;
      case 1:
        widget_height = (screen_height/8);
        widget_length = (3*screen_length/4);
        widget_x0 = (x0 + 1*screen_length/8);
        break;
      case 2:
        widget_height = (screen_height/6);
        widget_length = (3*screen_length/4);
        widget_x0 = (x0 + 1*screen_length/8);
        break;
      default:
        widget_height = (screen_height/8);
        widget_length = (2*screen_length/3);
        widget_x0 = (x0 + 1*screen_length/8);
        break;
    }
    switch(options_.position_index)
    {
      case 0:
        widget_y0 = (y0 - widget_height/2 + screen_height/6);
        break;
      case 1:
        widget_y0 = (y0 - widget_height/2 + screen_height/2);
        break;
      case 2:
        widget_y0 = (y0 - widget_height/2 + 5*screen_height/7);
        break;
      case 3:
        widget_y0 = (y0 - widget_height + screen_height);
        break;
      default:
        widget_y0 = (y0 - widget_height/2 + screen_height/2);
        break;
    }

    countdown_label_list_.append(new QLabel());
    QLabel* label = countdown_label_list_.last();

    label->setAttribute(Qt::WA_DeleteOnClose);
    // label->setAttribute(Qt::WA_TranslucentBackground);
    label->setTextFormat(Qt::RichText);
    label->setAutoFillBackground(true);
    label->setFixedSize(widget_length,widget_height);
    label->move(widget_x0,widget_y0);
    label->setAlignment(Qt::AlignCenter);
    label->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    label->installEventFilter(this);

    QPalette palette = label->palette();
    palette.setColor(QPalette::WindowText,ColorMessage::color_list_[options_.template_index][0]);
    palette.setColor(QPalette::Window,ColorMessage::color_list_[options_.template_index][1]);
    label->setPalette(palette);

    label->setText(widgetText());

    if(options_.enabled_screens.contains(i))
      label->show();
    else
      label->hide();
  }
}

void Countdown::updateLabels()
{
  for(QLabel* label : countdown_label_list_)
  {
    label->setText(widgetText());
  }
}

QString Countdown::widgetText(double ratio)
{
  QString html = "<span style=\"font-size:"+QString::number(ratio*options_.message_font_size)+"pt;";
  html += "align=\"center\";";
  html += ">";
  if (options_.bold) html += "<b>";
  if (options_.italic) html += "<i>";
  html += options_.message;
  if (options_.italic) html += "</i>";
  if (options_.bold) html += "</b>";

  html += "</p>";
  html += "<br /br>";
  html += "<span style=\"font-size:"+QString::number(ratio*options_.countdown_font_size)+"pt;";
  html += "align=\"center\";";
  html += ">";
  if(options_.bold) html += "<b>";
  if(options_.italic) html += "<i>";
  if(options_.countdown_duration.hour()>=1)
      html += options_.countdown_duration.toString("hh:mm:ss");
  else
      html += options_.countdown_duration.toString("mm:ss");
  if(options_.italic) html += "</i>";
  if(options_.bold) html += "</b>";
  html += "</p>";

  return html;
}

void Countdown::stop()
{
  state_ = State::Stopped;
  timer_countdown_running_.stop();
  timer_countdown_programmation_.stop();
  for(QLabel* label : countdown_label_list_)
    label->deleteLater();
  countdown_label_list_.clear();
  emit countdownStopped();
}
