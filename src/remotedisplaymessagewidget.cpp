#include "remotedisplaymessagewidget.h"
#include "ui_remotedisplaymessagewidget.h"

#include "QDebug"

RemoteDisplayMessageWidget::RemoteDisplayMessageWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::RemoteDisplayMessageWidget)
{
  ui->setupUi(this);
}

RemoteDisplayMessageWidget::~RemoteDisplayMessageWidget()
{
  delete ui;
}

void RemoteDisplayMessageWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Enter:
    case Qt::Key_Return:
      if(ui->message_textEdit_->hasFocus())
        on_send_pushButton__clicked();
      break;
    default:
      break;
    }
}

QStringList RemoteDisplayMessageWidget::message_list()
{
  return message_list_;
}

void RemoteDisplayMessageWidget::set_message_list(const QStringList & strL)
{
  updateRemoteDisplayMessages(strL);
}

void RemoteDisplayMessageWidget::on_send_pushButton__clicked()
{
  QString str;
  str += "$5;NORMAL;";
  str += ui->message_textEdit_->toPlainText();
  str += " \r\n";
  emit send_remoteDisplay_message(str);
  qDebug() << str;
}

void RemoteDisplayMessageWidget::on_urgent_pushButton__clicked()
{
  QString str;
  str += "$5;URGENT;";
  str += ui->message_textEdit_->toPlainText();
  str += " \r\n";
  emit send_remoteDisplay_message(str);
}

void RemoteDisplayMessageWidget::on_hidden_pushButton__clicked()
{
  QString str;
  str += "$5;HIDDEN;";
  str += ui->message_textEdit_->toPlainText();
  str += " \r\n";
  emit send_remoteDisplay_message(str);
}

void RemoteDisplayMessageWidget::on_delete_pushButton__clicked()
{
  QString str;
  str += "$5;CLEAR;";
  str += QString::number(ui->line_to_delete_spinBox_->value());
  str += "\r\n";
  emit send_remoteDisplay_message(str);
}

void RemoteDisplayMessageWidget::on_stored_message_comboBox__currentTextChanged(const QString & text)
{
  ui->message_textEdit_->setText(text);
}

void RemoteDisplayMessageWidget::updateRemoteDisplayMessages(const QStringList & strL)
{
  message_list_ = strL;
  ui->stored_message_comboBox_->clear();
  ui->stored_message_comboBox_->addItems(strL);
}

