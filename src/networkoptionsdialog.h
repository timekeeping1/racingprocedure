#ifndef NETWORKOPTIONSDIALOG_H
#define NETWORKOPTIONSDIALOG_H

#include <QDialog>
#include "network.h"
#include "elitebroadcastwidget.h"

namespace Ui {
  class NetworkOptionsDialog;
}

class NetworkOptionsDialog : public QDialog
{
  Q_OBJECT

public:
  NetworkOptionsDialog(Network&,EliteBroadcastWidget&,QWidget *parent = nullptr);
  ~NetworkOptionsDialog();

private slots:
  void on_buttonBox_accepted();

signals:
  void networkOptionsDialogAccepted(QString,QString,int);
  void networkOptionsDialogAccepted();
  void raceNotificationUpdated(const QString &);
  void raceNotificationEnabledUpdated(bool);

private:
  Ui::NetworkOptionsDialog *ui_;
  Network& network_;
  EliteBroadcastWidget& elite_broadcast_widget_;
};

#endif // NETWORKOPTIONSDIALOG_H
