#include "networkoptionsdialog.h"
#include "ui_networkoptionsdialog.h"
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QHostAddress>

NetworkOptionsDialog::NetworkOptionsDialog(Network& network,
                                           EliteBroadcastWidget& elite_broadcast_widget
                                           ,QWidget *parent) :
  QDialog(parent),
  ui_(new Ui::NetworkOptionsDialog),
  network_(network),
  elite_broadcast_widget_(elite_broadcast_widget)
{
  ui_->setupUi(this);

  if(network_.network_option()==Network::Mode::Local)
      ui_->local_radioButton->setChecked(true);
  else if(network_.network_option()==Network::Mode::Server)
      ui_->server_radioButton->setChecked(true);
  else if(network_.network_option()==Network::Mode::Client)
      ui_->client_radioButton->setChecked(true);

  ui_->ip_adresse_lineEdit->setText(network_.server_ip());
  ui_->server_port_spinBox->setValue(network_.server_port());
  ui_->client_port_spinBox->setValue(network_.client_port());

  ui_->connectToEliteCheckbox->setChecked(network_.connect_to_elite());
  ui_->elite_master_ip_lineEdit->setText(network_.elite_master_ip());
  ui_->elite_master_port_spinBox->setValue(network_.elite_master_port());

  ui_->race_notification_checkBox->setChecked(
        elite_broadcast_widget_.race_notification_enabled());
  ui_->race_notification_ratio_lineEdit->setText(
        elite_broadcast_widget_.race_notification());

  QList<QHostAddress> list = QNetworkInterface::allAddresses();
  QString str;
  for(QHostAddress x : list) {
      if((x.protocol()==QAbstractSocket::IPv4Protocol) && (!x.isLoopback()) )
        str += x.toString() + " / ";
  }
  str = str.left(str.size()-3);
  ui_->local_ip_label->setText(str);
}


NetworkOptionsDialog::~NetworkOptionsDialog()
{
  delete ui_;
}

void NetworkOptionsDialog::on_buttonBox_accepted()
{
  if(ui_->local_radioButton->isChecked())
      network_.set_network_option(Network::Mode::Local);
  else if(ui_->server_radioButton->isChecked())
      network_.set_network_option(Network::Mode::Server);
  else if(ui_->client_radioButton->isChecked())
      network_.set_network_option(Network::Mode::Client);

  network_.set_server_ip(ui_->ip_adresse_lineEdit->text());
  network_.set_server_port(ui_->server_port_spinBox->value());
  network_.set_client_port(ui_->client_port_spinBox->value());
  network_.set_connect_to_elite(ui_->connectToEliteCheckbox->isChecked());
  network_.set_elite_master_ip(ui_->elite_master_ip_lineEdit->text());
  network_.set_elite_master_port(ui_->elite_master_port_spinBox->value());

  elite_broadcast_widget_.set_race_notification_enabled(
        ui_->race_notification_checkBox->isChecked());
  elite_broadcast_widget_.set_race_notification(
        ui_->race_notification_ratio_lineEdit->text());

  emit networkOptionsDialogAccepted();
}
