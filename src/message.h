#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>
#include <QGuiApplication>
#include <QLabel>

class Message : public QObject
{
  Q_OBJECT

public:
  struct Options
  {
    QList<int> enabled_screens;
    int template_index;
    bool full_screen;
    int message_font_size;
    bool bold;
    bool italic;
    QString message;
  };

  static const QString options_str(const Options & o)
  {
    QString str;
    for(int x : o.enabled_screens)
      str += QString::number(x) + ",";
    if (str.lastIndexOf(",")>0)
      str.remove(str.lastIndexOf(","),1);
    str += ";";
    str += QString::number(o.template_index) + ";";
    str += QString::number(o.full_screen) + ";";
    str += QString::number(o.message_font_size) + ";";
    str += QString::number(o.bold) + ";";
    str += QString::number(o.italic) + ";";
    str += o.message + ";";
    return str;
  }

  enum State {Stopped, Displaying};
  Q_ENUM(State)

public:
  explicit Message(QObject *parent = nullptr);
  Message(const Options &,QObject *parent = nullptr);
  void keyPressEvent(QKeyEvent *event);
  bool eventFilter(QObject *object, QEvent *event);

  const Options &options() const;
  void set_options(const Options &);
  void set_enabled_screens(const QList<int> &);
  void set_template_index(int);
  void set_full_screen(bool);
  void set_message_font_size(int);
  void set_bold(bool);
  void set_italic(bool);
  void set_message(const QString &);
  State state();

signals:
  void displayStopped();

public:
  void display();
  void display(const Options &);
  void stop();

private:
  Options options_;
  QList<QLabel*> message_label_list_;
  State state_;

};

#endif // MESSAGE_H
