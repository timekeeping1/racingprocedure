#include "message.h"
#include "mainwindow.h"
#include <QScreen>

Message::Message(QObject *parent)
  : QObject(parent)
{
  state_ = State::Stopped;
}

Message::Message(const Options & options,QObject *parent)
  : QObject(parent)
{
  options_ = options;
}

void Message::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
      case Qt::Key_Enter:
        break;
      case Qt::Key_Return:
        break;
      case Qt::Key_Escape:
        stop();
        break;
      default:
        break;
    }
}

bool Message::eventFilter(QObject *object, QEvent *event)
{
    Q_UNUSED(object);
    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
    if (keyEvent->key() == Qt::Key_Escape)
    {
        stop();
        return true;
    } else return false;
}

const Message::Options &Message::options() const
{
  return options_;
}

void Message::set_options(const Options & options)
{
  options_ = options;
}

void Message::set_enabled_screens(const QList<int> & enabled_screens)
{
  options_.enabled_screens = enabled_screens;
}

void Message::set_template_index(int template_index)
{
  options_.template_index = template_index;
}

void Message::set_full_screen(bool full_screen)
{
  options_.full_screen = full_screen;
}

void Message::set_message_font_size(int message_font_size)
{
  options_.message_font_size = message_font_size;
}

void Message::set_bold(bool bold)
{
  options_.bold = bold;
}

void Message::set_italic(bool italic)
{
  options_.italic = italic;
}

void Message::set_message(const QString & message)
{
  options_.message = message;
}

Message::State Message::state()
{
  return state_;
}

void Message::display()
{
  state_ = State::Displaying;
  for(int i=0; i<qApp->screens().size();i++)
  {
    QScreen* screen = qApp->screens()[i];
    int screen_height = screen->size().height();
    int screen_length = screen->size().width();
    int x0 = screen->geometry().left();
    int y0 = screen->geometry().top();

    message_label_list_.append(new QLabel());
    QLabel* label = message_label_list_.last();
    label->setAttribute(Qt::WA_DeleteOnClose);
    label->setTextFormat(Qt::RichText);
    label->setAutoFillBackground(true);
    if(options_.full_screen)
    {
      label->setFixedSize(screen_length,screen_height);
      label->move(x0,y0);
    }
    else
    {
       label->setFixedSize(2*screen_length/3,screen_height/2);
       label->move(x0+(screen_length/6),y0+(screen_height/2)-(screen_height/4));
    }
    label->setAlignment(Qt::AlignCenter);
    label->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    label->installEventFilter(this);

    QPalette palette = label->palette();
    palette.setColor(QPalette::WindowText,ColorMessage::color_list_[options_.template_index][0]);
    palette.setColor(QPalette::Window,ColorMessage::color_list_[options_.template_index][1]);
    label->setPalette(palette);

    QString html = "<span style=\"font-size:"+QString::number(1.5*options_.message_font_size)+"pt;";
    html += "align=\"center\";";
    html += ">";
    if (options_.bold) html += "<b>";
    if (options_.italic) html += "<i>";
    for (QString x : options_.message.split("\n"))
    {
      html += x;
      html += "<br></br>";
      html += "<br></br>";
    }
    if (options_.italic) html += "</i>";
    if (options_.bold) html += "</b>";

    label->setText(html);

    if(options_.enabled_screens.contains(i))
      label->show();
    else
      label->hide();
  }
}

void Message::display(const Message::Options & options)
{
  set_options(options);
  display();
}

void Message::stop()
{
  state_ = State::Stopped;
  for(int i=0;i<message_label_list_.size();i++)
    message_label_list_[i]->deleteLater();
  for(QLabel* label : message_label_list_)
    label->deleteLater();
  message_label_list_.clear();
  emit displayStopped();
}
