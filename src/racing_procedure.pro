#-------------------------------------------------
#
# Project created by QtCreator 2019-05-12T09:40:28
#
#-------------------------------------------------

QT       += core gui widgets network svg multimedia

greaterThan(QT_MAJOR_VERSION, 4):

VERSION = 1.1.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

TARGET = racingProcedure
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        colormessage.cpp \
        countdown.cpp \
        elitebroadcastwidget.cpp \
        elitepilotspositionswidget.cpp \
        main.cpp \
        mainwindow.cpp \
        message.cpp \
        network.cpp \
        networkoptionsdialog.cpp \
        remotedisplaymessagewidget.cpp \
        storedmessagesdialog.cpp

HEADERS += \
        colormessage.h \
        countdown.h \
        elitebroadcastwidget.h \
        elitepilotspositionswidget.h \
        mainwindow.h \
        message.h \
        network.h \
        networkoptionsdialog.h \
        remotedisplaymessagewidget.h \
        storedmessagesdialog.h

FORMS += \
        elitebroadcastwidget.ui \
        elitepilotspositionswidget.ui \
        mainwindow.ui \
        networkoptionsdialog.ui \
        remotedisplaymessagewidget.ui \
        storedmessagesdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

DISTFILES += \
    CMakeLists.txt
