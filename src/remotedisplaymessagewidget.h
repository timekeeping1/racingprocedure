#ifndef REMOTEDISPLAYMESSAGEWIDGET_H
#define REMOTEDISPLAYMESSAGEWIDGET_H

#include <QWidget>
#include <QKeyEvent>

namespace Ui {
class RemoteDisplayMessageWidget;
}

class RemoteDisplayMessageWidget : public QWidget
{
  Q_OBJECT

public:
  explicit RemoteDisplayMessageWidget(QWidget *parent = nullptr);
  ~RemoteDisplayMessageWidget();
  void keyPressEvent(QKeyEvent *event);

  QStringList message_list();
  void set_message_list(const QStringList &);

private slots:
  void on_send_pushButton__clicked();
  void on_urgent_pushButton__clicked();
  void on_hidden_pushButton__clicked();
  void on_delete_pushButton__clicked();
  void on_stored_message_comboBox__currentTextChanged(const QString &arg1);
  void updateRemoteDisplayMessages(const QStringList &);

signals:
  void send_remoteDisplay_message(const QString &);

private:
  Ui::RemoteDisplayMessageWidget *ui;

  QStringList message_list_;
};

#endif // REMOTEDISPLAYMESSAGEWIDGET_H
