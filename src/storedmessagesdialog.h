#ifndef STOREDMESSAGESDIALOG_H
#define STOREDMESSAGESDIALOG_H

#include <QDialog>
#include "mainwindow.h"
#include <QListWidgetItem>

namespace Ui {
class StoredMessagesDialog;
}

class StoredMessagesDialog : public QDialog
{
  Q_OBJECT

public:
  explicit StoredMessagesDialog(const QList<ColorMessage> &,
                                const QStringList &,
                                QWidget *parent = nullptr);
  ~StoredMessagesDialog();

private slots:
  void on_import_countdown_file_pushButton_clicked();
  void on_import_remoteDisplay_file_pushButton_clicked();
  void on_delete_countdown_entry_pushButton_clicked();
  void on_new_countdown_entry_pushButton_clicked();
  void on_delete_remoteDisplay_entry_pushButton_clicked();
  void on_new_remoteDisplay_entry_pushButton_clicked();
  void on_buttonBox_accepted();
  void on_countdown_entry_up_pushButton_clicked();
  void on_countdown_entry_down_pushButton_clicked();
  void on_remote_display_up_pushButton_clicked();
  void on_remote_display_down_pushButton_clicked();
  void on_color_model_comboBox_currentIndexChanged(int index);

  void on_countdown_entry_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

signals:
  void updateCountdownMessages(const QList<ColorMessage> &);
  void updateRemoteDisplayMessages(const QStringList &);

private:
  Ui::StoredMessagesDialog *ui;
  QList<ColorMessage> countdown_list_;
  QStringList remoteDisplay_list_;
};

#endif // STOREDMESSAGESDIALOG_H
