#ifndef ELITEBROADCASTWIDGET_H
#define ELITEBROADCASTWIDGET_H

#include <QWidget>
#include <QTime>

namespace Ui {
class EliteBroadcastWidget;
}

class EliteBroadcastWidget : public QWidget
{
  Q_OBJECT

public:
  explicit EliteBroadcastWidget(QWidget *parent = nullptr);
  ~EliteBroadcastWidget();

  const QString & race_notification();
  bool race_notification_enabled();
  void reset();

private:
  void updateWidget();

private slots:
  void raceInformationDataReceived(const QString &);
  void on_sound_notification_checkBox__stateChanged(int arg1);

public slots:
  void set_race_notification(const QString &);
  void set_race_notification_enabled(bool);
  bool race_sound_notification_enabled();
  void set_race_sound_notification_enabled(bool);

private:
  Ui::EliteBroadcastWidget *ui;

  QString folder_;
  QString race_;
  QTime elapsed_time_;
  int elapsed_laps_;
  QTime remaining_time_;
  int remaining_laps_;
  QString race_flag_;
  int timestamp_elapsed_time_;
  QString folder_id_;
  QString race_id_;
  int race_type_;
  QString turn_yellow_sector_;
  int timestamp_packet_;
  bool frozen_elapsed_time_;
  bool race_notification_enabled_;
  QString race_notification_;
  bool race_sound_notification_enabled_;
  bool race_notification_raised_;
};

#endif // ELITEBROADCASTWIDGET_H
