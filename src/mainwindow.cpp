#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "networkoptionsdialog.h"

#include <QPixmap>
#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::MainWindow),
    status_icon_(new QLabel()),
    status_label_(new QLabel()),
    clients_label_(new QLabel()),
    reload_client_socket_pushbutton_(new QPushButton()),
    elite_connexion_label_(new QLabel()),
    reload_elite_connexion_pushbutton_(new QPushButton()),
    settings_(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0]+"/options.ini",QSettings::IniFormat)
{
    ui_->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);

    initVariables();
    initUI();

    connect(&timer_time_,                             SIGNAL(timeout()),                    this,SLOT(updateTime()));
    connect(ui_->countdown_position_comboBox,         SIGNAL(currentIndexChanged(int)),     this,SLOT(changeModel()));
    connect(ui_->countdown_size_comboBox,             SIGNAL(currentIndexChanged(int)),     this,SLOT(changeModel()));
    connect(ui_->bold_checkBox,                       SIGNAL(stateChanged(int)),            this,SLOT(changeModel()));
    connect(ui_->italic_checkBox,                     SIGNAL(stateChanged(int)),            this,SLOT(changeModel()));
    connect(ui_->countdown_message_font_size_spinBox, SIGNAL(valueChanged(int)),            this,SLOT(changeModel()));
    connect(ui_->countdown_font_size_spinBox,         SIGNAL(valueChanged(int)),            this,SLOT(changeModel()));
    connect(ui_->countdown_message_comboBox,          SIGNAL(currentTextChanged(QString)),  this,SLOT(changeModel()));
    connect(ui_->color_model_comboBox,                SIGNAL(currentIndexChanged(int)),     this,SLOT(changeModel()));

    connect(qApp,               SIGNAL(screenAdded(QScreen*)),  this,   SLOT(screenCountChanged(QScreen*)));
    connect(qApp,               SIGNAL(screenRemoved(QScreen*)),this,   SLOT(screenCountChanged(QScreen*)));

    connect(ui_->stop_pushButton,               SIGNAL(clicked()),  this, SLOT(stopDisplays()));
    connect(ui_->start_countdown_pushButton,    SIGNAL(clicked()),  this, SLOT(displayCountdown()));
    connect(ui_->program_countdown_pushButton,  SIGNAL(clicked()),  this, SLOT(programCountdown()));
    connect(ui_->add_to_countdown_pushButton,   SIGNAL(clicked()),  this, SLOT(addSubToCountdown()));
    connect(ui_->sub_to_countdown_pushButton,   SIGNAL(clicked()),  this, SLOT(addSubToCountdown()));
    connect(ui_->send_full_screen_message_pushButton, SIGNAL(clicked()),  this,   SLOT(displayFullScreenMessage()));
    connect(ui_->send_popup_message_pushButton,       SIGNAL(clicked()),  this,   SLOT(displayPopUpMessage()));

    connect(ui_->identify_screens_pushButton,  SIGNAL(pressed()),      this,   SLOT(identifyScreens()));
    connect(ui_->identify_screens_pushButton,  SIGNAL(released()),     this,   SLOT(stopIdentifyScreens()));

    connect(&message_widget_,   SIGNAL(displayStopped()),                   this, SLOT(stopDisplays()));
    connect(&countdown_widget_, SIGNAL(countdownStopped()),                 this, SLOT(stopDisplays()));
    connect(&countdown_widget_, SIGNAL(countdownProgrammed(const QTime &)), this, SLOT(updateCountdownUI(const QTime &)));
    connect(&countdown_widget_, SIGNAL(countdownRunning()),                 this, SLOT(updateCountdownUI()));
    connect(&countdown_widget_, SIGNAL(programmingCountdownError(const QString &)), this, SLOT(displayWarningBox(const QString &)));
    connect(&countdown_widget_, SIGNAL(countdownReadyToDisplay()),          this, SLOT(displayCountdown()));
    connect(this,               SIGNAL(updateNetwork()),                    &network_,    SLOT(updateNetwork()));
    connect(this,               SIGNAL(updateNetwork()),                    &network_,    SLOT(updateEliteNetwork()));
    connect(this,               SIGNAL(updateNetwork()),                      &network_,    SLOT(clearBuffers()));

    connect(&network_,SIGNAL(updateNetworkUI()),            this,SLOT(updateNetworkUI()));
    connect(&network_,SIGNAL(identifyScreens()),            this,SLOT(identifyScreens()));
    connect(&network_,SIGNAL(stopIdentifyScreens()),        this,SLOT(stopIdentifyScreens()));
    connect(&network_,SIGNAL(addSubToCountdown(QTime,bool)),this,SLOT(addSubToCountdown(QTime,bool)));
    connect(&network_,SIGNAL(stopDisplaying()),             this,SLOT(stopDisplays()));
    connect(&network_,SIGNAL(displayCountdown(Countdown::Options)), this,SLOT(displayCountdown(Countdown::Options)));
    connect(&network_,SIGNAL(displayMessage(Message::Options)),     this,SLOT(displayMessage(Message::Options)));
    connect(&network_,SIGNAL(updateEnabledScreens(int,QList<int>)),     this,SLOT(updateEnabledScreensWidget(int,QList<int>)));
    connect(&network_,SIGNAL(updateInformation(const QString &, const QString &)),
              this,SLOT(updateNetworkCommandLabel(const QString &, const QString &)));
    connect(&network_,SIGNAL(resetUI()),                    this,SLOT(EnableMainSettings()));
    connect(&network_,SIGNAL(updateEnabledScreens()),       this,SLOT(sendTcpUpdateEnabledScreens()));

    connect(&network_,SIGNAL(raceInformationDataReceived(const QString &)), &elite_pilots_positions_widget_,SLOT(raceInformationDataReceived(const QString &)));
    connect(&network_,SIGNAL(instantPassingsDataReceived(const QString &)), &elite_pilots_positions_widget_,SLOT(instantPassingsDataReceived(const QString &)));

    emit updateNetwork();
}

MainWindow::~MainWindow()
{
    delete ui_;
}

void MainWindow::initVariables()
{
  QStringList strL = settings_.value("enabled_screens",0).toString().split(",");
  QList<int> enabled_screens;
  for(int i=0;i<strL.size();i++)
    enabled_screens.append(strL[i].toInt());

  countdown_widget_.set_enabled_screens(enabled_screens);
  countdown_widget_.set_template_index(settings_.value("model",2).toInt());
  countdown_widget_.set_position_index(settings_.value("position",2).toInt());
  countdown_widget_.set_size_index(settings_.value("size",1).toInt());
  countdown_widget_.set_message_font_size(settings_.value("textsize",36).toInt());
  countdown_widget_.set_countdown_font_size(settings_.value("countdownsize",40).toInt());
  countdown_widget_.set_bold(settings_.value("bold",true).toBool());
  countdown_widget_.set_italic(settings_.value("italic",false).toBool());
  countdown_widget_.set_time_countdown_end(settings_.value("time",QTime(8,0,0,0)).toTime());
  countdown_widget_.set_countdown_duration(settings_.value("duration",QTime(0,5,0,0)).toTime());
  countdown_widget_.set_time_countdown_start(
        countdown_widget_.options().time_countdown_end.addSecs(
          -60*countdown_widget_.options().countdown_duration.minute()
          - countdown_widget_.options().countdown_duration.second()));
  countdown_widget_.set_message(settings_.value("countdownMessage","OUVERTURE VOIE DES STANDS :").toString());

  message_widget_.set_enabled_screens(enabled_screens);
  message_widget_.set_template_index(settings_.value("model",2).toInt());
  message_widget_.set_full_screen(false);
  message_widget_.set_message_font_size(settings_.value("textsize",36).toInt());
  message_widget_.set_bold(settings_.value("bold",true).toBool());
  message_widget_.set_italic(settings_.value("italic",false).toBool());
  message_widget_.set_message(settings_.value("message","Message").toString());

  countdown_message_list_  = ColorMessage::listFromSettings(
        settings_.value("countdownMessages",
                        QStringList(
                                 {"OUVERTURE VOIE DES STANDS :\\2",
                                  "FERMETURE VOIE DES STANDS :\\6",
                                  "\\0"} )).toStringList());

  remote_display_widget_.set_message_list(
        settings_.value("remoteDisplayMessages").toStringList());

  settings_.beginGroup("network");
  QString str = settings_.value("option", "local").toString();
  if (str == "local")
    network_.set_network_option(Network::Mode::Local);
  else if (str == "server")
    network_.set_network_option(Network::Mode::Server);
  else if (str == "client")
    network_.set_network_option(Network::Mode::Client);
  network_.set_server_ip(settings_.value("ip", "127.0.0.1").toString());
  network_.set_server_port(settings_.value("serverPort", 14500).toInt());
  network_.set_client_port(settings_.value("clientPort", 14500).toInt());
  network_.set_connect_to_elite(settings_.value("connect_to_elite", false).toBool());
  network_.set_elite_master_ip(settings_.value("elite_ip", "127.0.0.1").toString());
  network_.set_elite_master_port(settings_.value("elite_port", 14000).toInt());
  elite_broadcast_widget_.set_race_notification(
        settings_.value("race_notification", "2/3").toString());
  elite_broadcast_widget_.set_race_notification_enabled(
        settings_.value("race_notification_enabled",true).toBool());
  elite_broadcast_widget_.set_race_sound_notification_enabled(
        settings_.value("race_sound_notification",false).toBool());
  settings_.endGroup();
}

void MainWindow::initUI()
{
    setWindowTitle(QString("Racing Procedure")+QString(" Version ")+ APP_VERSION);

    timer_time_.start(100);

    ui_->statusBar->addWidget(status_icon_);
    ui_->statusBar->addWidget(status_label_);
    ui_->statusBar->addWidget(clients_label_);
    ui_->statusBar->addWidget(reload_client_socket_pushbutton_);
    ui_->statusBar->addWidget(elite_connexion_label_);
    ui_->statusBar->addWidget(reload_elite_connexion_pushbutton_);
    status_icon_->setFixedWidth(40);
    status_label_->setFixedWidth(80);
    clients_label_->setFixedWidth(100);
    reload_client_socket_pushbutton_->setFixedWidth(30);
    reload_client_socket_pushbutton_->setIcon(
          QPixmap(":/images/sync.png").scaledToWidth(30));
    elite_connexion_label_->setFixedWidth(120);
    reload_elite_connexion_pushbutton_->setFixedWidth(30);
    reload_elite_connexion_pushbutton_->setIcon(
          QPixmap(":/images/sync.png").scaledToWidth(30));

    ui_->eliteInfosLayout->insertWidget(0,&elite_broadcast_widget_);
    ui_->pilotPositionVerticalLayout->insertWidget(0,&elite_pilots_positions_widget_);
    ui_->eliteInfosLayout->insertWidget(2,&remote_display_widget_);
    //ui_->remote_display_layout->insertWidget(1,&remote_display_widget_);
    elite_broadcast_widget_.hide();
    elite_pilots_positions_widget_.hide();
    remote_display_widget_.hide();

    ui_->add_to_countdown_pushButton->setEnabled(false);
    ui_->sub_to_countdown_pushButton->setEnabled(false);

    Countdown::Options co = countdown_widget_.options();
    Message::Options mo = message_widget_.options();

    updateEnabledScreensWidget(qApp->screens().size(),co.enabled_screens);

    ui_->color_model_comboBox->blockSignals(true);
    for(int i=0;i<ColorMessage::color_list_.size();i++)
    {
        ui_->color_model_comboBox->addItem("Texte");
        ui_->color_model_comboBox->setItemData(i,QBrush(ColorMessage::color_list_[i][0]), Qt::ForegroundRole);
        ui_->color_model_comboBox->setItemData(i,QBrush(ColorMessage::color_list_[i][1]), Qt::BackgroundRole);
    }
    QPalette palette = ui_->color_model_comboBox->palette();
    palette.setColor(QPalette::ButtonText,ColorMessage::color_list_[co.template_index][0]);
    palette.setColor(QPalette::Button,ColorMessage::color_list_[co.template_index][1]);
    ui_->color_model_comboBox->setPalette(palette);
    ui_->color_model_comboBox->setStyleSheet("color: "+ColorMessage::color_name_list_[co.template_index][0] +
                "; background-color: "+ColorMessage::color_name_list_[co.template_index][1]);

    ui_->color_model_comboBox->blockSignals(false);

    updateCountdownMessages(countdown_message_list_);

    ui_->color_model_comboBox->setCurrentIndex(co.template_index);
    ui_->countdown_position_comboBox->setCurrentIndex(co.position_index);
    ui_->countdown_size_comboBox->setCurrentIndex(co.size_index);
    ui_->countdown_message_font_size_spinBox->setValue(co.message_font_size);
    ui_->countdown_font_size_spinBox->setValue(co.countdown_font_size);
    ui_->bold_checkBox->setChecked(co.bold);
    ui_->italic_checkBox->setChecked(co.bold);
    ui_->time_start_countdown_timeEdit->setTime(co.time_countdown_start);
    ui_->time_end_countdown_timeEdit->setTime(co.time_countdown_end);
    ui_->duration_lineEdit->setTime(co.countdown_duration);
    ui_->countdown_message_comboBox->setCurrentText(co.message);
    ui_->message_textEdit->setText(mo.message);

    ui_->time_start_countdown_label->setText("--");
    ui_->time_remaining_before_countdown_start_label->setText("--");
    ui_->duration_label->setText("--");

    ui_->preview_label->setAutoFillBackground(true);
    ui_->preview_label->setAlignment(Qt::AlignCenter);

    ui_->preview_label->setText(countdown_widget_.widgetText(0.5));

    setCountdownPreviewStyle();
    setMessagePreviewStyle();
}

void MainWindow::updateEnabledScreensWidget(int screen_count, QList<int> enabled_screens)
{
  const QSignalBlocker blocker(this);
  countdown_widget_.set_enabled_screens(enabled_screens);
  message_widget_.set_enabled_screens(enabled_screens);

  for(QCheckBox* x : enabled_screens_checkBox_list_)
    x->deleteLater();
  enabled_screens_checkBox_list_.clear();
  for(int i=0;i<screen_count;i++)
  {
    enabled_screens_checkBox_list_.append(new QCheckBox(QString("Ecran "+QString::number(i+1)),this));
    ui_->gridLayout->addWidget(enabled_screens_checkBox_list_[i],i/3,i%3);
    if(enabled_screens.contains(i))
      enabled_screens_checkBox_list_[i]->setChecked(true);
    connect(this->enabled_screens_checkBox_list_[i],SIGNAL(stateChanged(int)),this,SLOT(changeEnabledScreens()));
  }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Enter:
    case Qt::Key_Return:
        if(ui_->time_start_countdown_timeEdit->hasFocus())
          programCountdown();
        if(ui_->time_end_countdown_timeEdit->hasFocus())
          programCountdown();
        if(ui_->duration_lineEdit->hasFocus())
          programCountdown();
        if(ui_->countdown_message_comboBox->hasFocus())
          programCountdown();
        if(ui_->message_textEdit->hasFocus())
          displayPopUpMessage();
        break;
    case Qt::Key_Escape:
        stopDisplays();
        break;
    default:
        break;
    }
}

void MainWindow::closeEvent ( QCloseEvent * event )
{
  settings_.setValue("model",ui_->color_model_comboBox->currentIndex());
  settings_.setValue("position",ui_->countdown_position_comboBox->currentIndex());
  settings_.setValue("size",ui_->countdown_size_comboBox->currentIndex());
  settings_.setValue("textsize",ui_->countdown_message_font_size_spinBox->value());
  settings_.setValue("countdownsize",ui_->countdown_font_size_spinBox->value());
  settings_.setValue("bold",ui_->bold_checkBox->isChecked());
  settings_.setValue("italic",ui_->italic_checkBox->isChecked());
  settings_.setValue("countdownMessage",ui_->countdown_message_comboBox->currentText());
  settings_.setValue("countdownMessages",ColorMessage::toSettings(countdown_message_list_));
  settings_.setValue("remoteDisplayMessages",remote_display_widget_.message_list());
  settings_.setValue("time",ui_->time_end_countdown_timeEdit->time());
  settings_.setValue("duration",ui_->duration_lineEdit->time());
  settings_.setValue("message",ui_->message_textEdit->toPlainText());

  QString list;
  QList<int> enabled_screens = countdown_widget_.options().enabled_screens;
  for(int i=0;i<qApp->screens().size();i++)
    if(enabled_screens.contains(i))
      list+= QString::number(i)+",";
  if(!list.isEmpty()) list.remove(list.size()-1,1);
  settings_.setValue("enabled_screens",list);

  settings_.beginGroup("network");
  settings_.setValue("option", network_.network_option_str());
  settings_.setValue("ip", network_.server_ip());
  settings_.setValue("serverPort", network_.server_port());
  settings_.setValue("ClientPort", network_.client_port());
  settings_.setValue("connect_to_elite", network_.connect_to_elite());
  settings_.setValue("elite_ip", network_.elite_master_ip());
  settings_.setValue("elite_port", network_.elite_master_port());
  settings_.setValue("race_notification",elite_broadcast_widget_.race_notification());
  settings_.setValue("race_notification_enabled",elite_broadcast_widget_.race_notification_enabled());
  settings_.setValue("race_sound_notification",elite_broadcast_widget_.race_sound_notification_enabled());
  settings_.endGroup();

  event->accept();
}

bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
  Q_UNUSED(object);
  QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
  if (keyEvent->key() == Qt::Key_Escape)
  {
      stopDisplays();
      return true;
  } else return false;
}

void MainWindow::updateTime()
{
  timer_time_.start(100);
  ui_->actual_time_label->setText(QTime::currentTime().toString());
}

void MainWindow::screenCountChanged(QScreen*)
{
  if(network_.network_option()==Network::Mode::Local)
    updateEnabledScreensWidget(qApp->screens().size(),countdown_widget_.options().enabled_screens);
  if(network_.network_option()==Network::Mode::Server)
    network_.serverCommandUpdateEnabledScreens(
          qApp->screens().size(),countdown_widget_.options().enabled_screens);
}

void MainWindow::changeEnabledScreens()
{
  QList<int> enabled_screens;
  for(int i=0;i<enabled_screens_checkBox_list_.size();i++)
    if(enabled_screens_checkBox_list_[i]->isChecked()) enabled_screens.append(i);

  countdown_widget_.set_enabled_screens(enabled_screens);
  message_widget_.set_enabled_screens(enabled_screens);
}

void MainWindow::changeModel()
{
  if(static_cast<QComboBox*>(sender()) == ui_->color_model_comboBox)
  {
    int index = ui_->color_model_comboBox->currentIndex();
    QPalette palette = ui_->color_model_comboBox->palette();
    palette.setColor(QPalette::ButtonText,ColorMessage::color_list_[index][0]);
    palette.setColor(QPalette::Button,ColorMessage::color_list_[index][1]);
    ui_->color_model_comboBox->setPalette(palette);
    ui_->color_model_comboBox->setStyleSheet(
          "color: "+ColorMessage::color_name_list_[index][0] +
          "; background-color: "+ColorMessage::color_name_list_[index][1]);

    countdown_widget_.set_template_index(index);
    message_widget_.set_template_index(index);

    if(ui_->countdown_message_comboBox->currentIndex()>-1)
      countdown_message_list_[ui_->countdown_message_comboBox->currentIndex()]
        .set_color_model_index(index);
    ui_->countdown_message_comboBox->setItemData(
          ui_->countdown_message_comboBox->currentIndex(),
          QBrush(ColorMessage::color_list_[index][0]) ,Qt::ForegroundRole);
    ui_->countdown_message_comboBox->setItemData(
          ui_->countdown_message_comboBox->currentIndex(),
          QBrush(ColorMessage::color_list_[index][1]), Qt::BackgroundRole);
    palette = ui_->countdown_message_comboBox->palette();
    palette.setColor(QPalette::ButtonText,ColorMessage::color_list_[index][0]);
    palette.setColor(QPalette::Button,ColorMessage::color_list_[index][1]);
    ui_->countdown_message_comboBox->setPalette(palette);
    ui_->countdown_message_comboBox->setStyleSheet(
          "color: "+ColorMessage::color_name_list_[index][0] +
          "; background-color: "+ColorMessage::color_name_list_[index][1]);
  }
  else if (static_cast<QComboBox*>(sender()) == ui_->countdown_message_comboBox)
  {
    if(ui_->countdown_message_comboBox->currentIndex()>=0)
      ui_->color_model_comboBox->setCurrentIndex(
            countdown_message_list_[ui_->countdown_message_comboBox->currentIndex()]
            .color_model_index());
  }
  QList<int> enabled_screens;
  for(int i=0;i<enabled_screens_checkBox_list_.size();i++)
    if(enabled_screens_checkBox_list_[i]->isChecked()) enabled_screens.append(i);

  message_widget_.set_enabled_screens(enabled_screens);
  message_widget_.set_template_index(ui_->color_model_comboBox->currentIndex());
  message_widget_.set_message_font_size(ui_->countdown_message_font_size_spinBox->value());
  message_widget_.set_bold(ui_->bold_checkBox->isChecked());
  message_widget_.set_italic(ui_->italic_checkBox->isChecked());
  message_widget_.set_message(ui_->message_textEdit->toPlainText());

  countdown_widget_.set_enabled_screens(enabled_screens);
  countdown_widget_.set_template_index(ui_->color_model_comboBox->currentIndex());
  countdown_widget_.set_position_index(ui_->countdown_position_comboBox->currentIndex());
  countdown_widget_.set_size_index(ui_->countdown_size_comboBox->currentIndex());
  countdown_widget_.set_message_font_size(ui_->countdown_message_font_size_spinBox->value());
  countdown_widget_.set_countdown_font_size(ui_->countdown_font_size_spinBox->value());
  countdown_widget_.set_bold(ui_->bold_checkBox->isChecked());
  countdown_widget_.set_italic(ui_->italic_checkBox->isChecked());
  countdown_widget_.set_time_countdown_start(ui_->time_start_countdown_timeEdit->time());
  countdown_widget_.set_time_countdown_end(ui_->time_end_countdown_timeEdit->time());
  countdown_widget_.set_countdown_duration(ui_->duration_lineEdit->time());
  countdown_widget_.set_message(ui_->countdown_message_comboBox->currentText());

  add_sub_ = ui_->add_sub_to_countdown_timeEdit->time();

  ui_->preview_label->setText(countdown_widget_.widgetText(0.5));

  setCountdownPreviewStyle();
  setMessagePreviewStyle();
}

void MainWindow::setCountdownPreviewStyle()
{
  const QSignalBlocker blocker(this);
  Countdown::Options co = countdown_widget_.options();

  QPalette palette = ui_->preview_label->palette();
  palette.setColor(QPalette::WindowText,ColorMessage::color_list_[co.template_index][0]);
  palette.setColor(QPalette::Window,ColorMessage::color_list_[co.template_index][1]);
  ui_->preview_label->setPalette(palette);
}

void MainWindow::setMessagePreviewStyle()
{
  const QSignalBlocker blocker(this);
  Message::Options mo = message_widget_.options();

  QPalette palette = ui_->message_textEdit->palette();
  palette.setColor(QPalette::Text,ColorMessage::color_list_[mo.template_index][0]);
  palette.setColor(QPalette::Base,ColorMessage::color_list_[mo.template_index][1]);
  ui_->message_textEdit->setPalette(palette);
  ui_->message_textEdit->selectAll();
  ui_->message_textEdit->setAlignment(Qt::AlignCenter|Qt::AlignVCenter);
  if(mo.bold)
      ui_->message_textEdit->setFontWeight(QFont::Bold);
  else
      ui_->message_textEdit->setFontWeight(QFont::Normal);
  if(mo.italic)
      ui_->message_textEdit->setFontItalic(true);
  else
      ui_->message_textEdit->setFontItalic(false);
  ui_->message_textEdit->setFontPointSize(0.5*mo.message_font_size);
  QTextCursor cursor = ui_->message_textEdit->textCursor();
  cursor.movePosition(QTextCursor::End);
  ui_->message_textEdit->setTextCursor(cursor);
}

void MainWindow::EnableMainSettings()
{
  ui_->gridLayout->setEnabled(true);
  ui_->color_model_comboBox->setEnabled(true);
  ui_->countdown_position_comboBox->setEnabled(true);
  ui_->countdown_size_comboBox->setEnabled(true);
  ui_->countdown_message_font_size_spinBox->setEnabled(true);
  ui_->countdown_font_size_spinBox->setEnabled(true);
  ui_->bold_checkBox->setEnabled(true);
  ui_->italic_checkBox->setEnabled(true);
  ui_->time_start_countdown_timeEdit->setEnabled(true);
  ui_->time_end_countdown_timeEdit->setEnabled(true);
  ui_->duration_lineEdit->setEnabled(true);
  ui_->countdown_message_comboBox->setEnabled(true);
  ui_->start_countdown_pushButton->setEnabled(true);
  ui_->program_countdown_pushButton->setEnabled(true);
  ui_->identify_screens_pushButton->setEnabled(true);
  ui_->send_full_screen_message_pushButton->setEnabled(true);
  ui_->send_popup_message_pushButton->setEnabled(true);

  ui_->add_to_countdown_pushButton->setEnabled(false);
  ui_->sub_to_countdown_pushButton->setEnabled(false);
}

void MainWindow::disableMainSettings()
{
  ui_->gridLayout->setEnabled(false);
  ui_->color_model_comboBox->setEnabled(false);
  ui_->countdown_position_comboBox->setEnabled(false);
  ui_->countdown_size_comboBox->setEnabled(false);
  ui_->countdown_message_font_size_spinBox->setEnabled(false);
  ui_->countdown_font_size_spinBox->setEnabled(false);
  ui_->bold_checkBox->setEnabled(false);
  ui_->italic_checkBox->setEnabled(false);
  ui_->time_start_countdown_timeEdit->setEnabled(false);
  ui_->time_end_countdown_timeEdit->setEnabled(false);
  ui_->duration_lineEdit->setEnabled(false);
  ui_->countdown_message_comboBox->setEnabled(false);
  ui_->start_countdown_pushButton->setEnabled(false);
  ui_->program_countdown_pushButton->setEnabled(false);
  ui_->identify_screens_pushButton->setEnabled(false);
  ui_->send_full_screen_message_pushButton->setEnabled(false);
  ui_->send_popup_message_pushButton->setEnabled(false);

  ui_->add_to_countdown_pushButton->setEnabled(true);
  ui_->sub_to_countdown_pushButton->setEnabled(true);
}

void MainWindow::stopDisplays()
{ 
  if(message_widget_.state() == Message::State::Displaying)
    message_widget_.stop();
  if((countdown_widget_.state() == Countdown::State::Displaying)
     || (countdown_widget_.state() == Countdown::State::Programmed))
    countdown_widget_.stop();

  changeModel();
  EnableMainSettings();

  ui_->time_start_countdown_label->setText("--");
  ui_->time_remaining_before_countdown_start_label->setText("--");
  ui_->duration_label->setText("--");

  if(network_.network_option() == Network::Mode::Client)
  {
    if(network_.clientSocketState() == QAbstractSocket::ConnectedState)
      network_.clientCommandStop();
    else
      QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
  else if(network_.network_option() == Network::Mode::Server)
  {
    network_.serverCommandStop();
  }
}

void MainWindow::displayCountdown()
{
  disableMainSettings();
  ui_->time_start_countdown_label->setText("Envoyé");
  ui_->time_remaining_before_countdown_start_label->setText("Envoyé");
  QTime duration = countdown_widget_.options().countdown_duration;
  ui_->duration_label->setText(duration.toString());
  if((network_.network_option()==Network::Mode::Local)||(network_.network_option()==Network::Mode::Server))
  {
    countdown_widget_.displayCountdown();
  }
  if(network_.network_option()==Network::Mode::Client)
  {
      if(network_.clientSocketState() == QAbstractSocket::ConnectedState)
          network_.clientCommandDisplayCountdown(countdown_widget_.options());
      else
          QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
}

void MainWindow::displayCountdown(Countdown::Options countdown_options)
{
  countdown_widget_.set_options(countdown_options);
  displayCountdown();
}

void MainWindow::programCountdown()
{
  disableMainSettings();
  ui_->time_start_countdown_label->setText(ui_->time_start_countdown_timeEdit->text());

  countdown_widget_.programCountdown();
  if(network_.network_option()==Network::Mode::Client)
  {
      if(network_.clientSocketState() == QAbstractSocket::ConnectedState)
          network_.clientCommandProgramCountdown(countdown_widget_.options());
      else
          QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
}

void MainWindow::identifyScreens()
{
  if(network_.network_option() == Network::Mode::Client)
  {
    if(network_.clientSocketState() == QAbstractSocket::ConnectedState)
      network_.clientCommandIdentifyScreens();
    else
      QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
  else
  {
    QPalette palette;
    screen_id_.clear();
    for(int i=0;i<qApp->screens().size();i++)
    {
      screen_id_.append(new QLabel());
      screen_id_[i]->setAttribute(Qt::WA_DeleteOnClose);
      screen_id_[i]->setFrameStyle(QFrame::StyledPanel);
      screen_id_[i]->setFixedSize(150,150);
      screen_id_[i]->move(qApp->screens()[i]->geometry().left()+100,
                        qApp->screens()[i]->geometry().top()+100);
      screen_id_[i]->setAlignment(Qt::AlignCenter);
      screen_id_[i]->setFont(QFont("Arial",100,QFont::Bold));
      screen_id_[i]->setWindowFlags(Qt::FramelessWindowHint| Qt::WindowStaysOnTopHint);
      screen_id_[i]->setAttribute(Qt::WA_ShowWithoutActivating);
      palette = screen_id_[i]->palette();
      palette.setColor(QPalette::WindowText,Qt::black);
      palette.setColor(QPalette::Window,Qt::white);
      screen_id_[i]->setPalette(palette);
      screen_id_[i]->setText(QString::number(i+1));
      screen_id_[i]->show();
    }
  }
}

void MainWindow::stopIdentifyScreens()
{
  if(network_.network_option() == Network::Mode::Client)
  {
    if(network_.clientSocketState()==QAbstractSocket::ConnectedState)
      network_.clientCommandStopIdentifyScreens();
    else
      QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
  else
  {
    for(int i=0;i<qApp->screens().size();i++)
    {
      screen_id_[i]->close();
    }
  }
}

void MainWindow::addSubToCountdown(QTime delta, bool add)
{
  if(network_.network_option() == Network::Mode::Client)
  {
    QString str = "addSubToCountdown;";
    if(network_.clientSocketState() ==QAbstractSocket::ConnectedState)
    {
      network_.clientCommandAddSubToCountdown(delta, add);
    }
    else
      QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
  else
  {
    countdown_widget_.addSubToCountdown(delta,add);
  }
}

void MainWindow::addSubToCountdown()
{
  bool add;
  if(static_cast<QPushButton*>(sender()) == ui_->add_to_countdown_pushButton)
    add = true;
  if(static_cast<QPushButton*>(sender()) == ui_->sub_to_countdown_pushButton)
    add = false;
  add_sub_ = ui_->add_sub_to_countdown_timeEdit->time();
  if(network_.network_option() == Network::Mode::Client)
  {
    QString str = "addSubToCountdown;";
    if(network_.clientSocketState() ==QAbstractSocket::ConnectedState)
    {
      network_.clientCommandAddSubToCountdown(add_sub_, add);
    }
    else
      QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
  else
  {
    countdown_widget_.addSubToCountdown(add_sub_,add);
  }
}

void MainWindow::displayFullScreenMessage()
{
  message_widget_.set_full_screen(true);
  message_widget_.set_message(ui_->message_textEdit->toPlainText());
  disableMainSettings();
  if(network_.network_option() == Network::Mode::Client)
  {
    if(network_.clientSocketState()==QAbstractSocket::ConnectedState)
    {
      network_.clientCommandDisplayMessage(message_widget_.options());
    }
    else
      QMessageBox::warning(this, "Erreur", "Serveur introuvable");
  }
  else
  {
    message_widget_.display();
  }
}

void MainWindow::displayPopUpMessage()
{
    disableMainSettings();
    message_widget_.set_message(ui_->message_textEdit->toPlainText());
    message_widget_.set_full_screen(false);
    if(network_.network_option() == Network::Mode::Client)
    {
      if(network_.clientSocketState()==QAbstractSocket::ConnectedState)
      {
        network_.clientCommandDisplayMessage(message_widget_.options());
      }
      else
        QMessageBox::warning(this, "Erreur", "Serveur introuvable");
    }
    else
    { 
      message_widget_.display();
    }
}

void MainWindow::displayMessage(Message::Options message_options)
{
    disableMainSettings();
    message_widget_.display(message_options);
}

void MainWindow::on_time_start_countdown_timeEdit_userTimeChanged(const QTime &time)
{
  int duration_int = 60*ui_->duration_lineEdit->time().minute() + ui_->duration_lineEdit->time().second();
  ui_->time_end_countdown_timeEdit->setTime(time.addSecs(duration_int));
  countdown_widget_.set_time_countdown_start(time);
  countdown_widget_.set_time_countdown_end(time.addSecs(duration_int));
}

void MainWindow::on_time_end_countdown_timeEdit_userTimeChanged(const QTime &time)
{
  int duration_int = 60*ui_->duration_lineEdit->time().minute() + ui_->duration_lineEdit->time().second();
  ui_->time_start_countdown_timeEdit->setTime(time.addSecs(-duration_int));
  countdown_widget_.set_time_countdown_start(time.addSecs(-duration_int));
  countdown_widget_.set_time_countdown_end(time);
}

void MainWindow::on_duration_lineEdit_timeChanged(const QTime &duration)
{
  countdown_widget_.set_countdown_duration(duration);
  int duration_int = 60*duration.minute() + duration.second();
  countdown_widget_.set_time_countdown_start(countdown_widget_.options().time_countdown_end.addSecs(-duration_int));
  ui_->time_start_countdown_timeEdit->setTime(countdown_widget_.options().time_countdown_start);
  ui_->preview_label->setText(countdown_widget_.widgetText(0.5));
}

void MainWindow::on_actionReseau_triggered()
{
  NetworkOptionsDialog* dialog = new NetworkOptionsDialog(network_, elite_broadcast_widget_);
  dialog->show();
  dialog->raise();
  dialog->activateWindow();
  connect(dialog, SIGNAL(networkOptionsDialogAccepted()),&network_,SLOT(updateNetwork()));
  connect(dialog, SIGNAL(networkOptionsDialogAccepted()),&network_,SLOT(updateEliteNetwork()));
}

void MainWindow::on_actionMessages_enregistres_triggered()
{
  StoredMessagesDialog* dialog = new StoredMessagesDialog(
        countdown_message_list_,remote_display_widget_.message_list());
  dialog->show();
  dialog->raise();
  dialog->activateWindow();
  connect(dialog,SIGNAL(updateCountdownMessages(const QList<ColorMessage> &)),
          this,SLOT(updateCountdownMessages(const QList<ColorMessage> &)));
  connect(dialog,SIGNAL(updateRemoteDisplayMessages(const QStringList &)),
          &remote_display_widget_,SLOT(updateRemoteDisplayMessages(const QStringList &)));
}

void MainWindow::updateCountdownUI(const QTime & remaining_time)
{
    ui_->time_start_countdown_label->setText(
          countdown_widget_.options().time_countdown_start.toString());
    ui_->time_remaining_before_countdown_start_label->setText(remaining_time.toString());
    ui_->duration_label->setText(
          countdown_widget_.options().countdown_duration.toString());
}

void MainWindow::updateCountdownUI()
{
  ui_->time_start_countdown_label->setText("Envoyé");
  ui_->time_remaining_before_countdown_start_label->setText("Envoyé");
  ui_->duration_label->setText(
        countdown_widget_.options().countdown_duration.toString());
}

void MainWindow::updateCountdownMessages(const QList<ColorMessage> & list)
{
  int i = ui_->countdown_message_comboBox->currentIndex();
  int index = countdown_widget_.options().template_index;
  countdown_message_list_ = list;
  ui_->countdown_message_comboBox->clear();

  for (ColorMessage x : countdown_message_list_)
  {
    ui_->countdown_message_comboBox->addItem(x.message());

    ui_->countdown_message_comboBox->setItemData(
          ui_->countdown_message_comboBox->count()-1,
          QBrush(x.font_color()) ,Qt::ForegroundRole);
    ui_->countdown_message_comboBox->setItemData(
          ui_->countdown_message_comboBox->count()-1,
          QBrush(x.background_color()),Qt::BackgroundRole);
    QPalette palette = ui_->countdown_message_comboBox->palette();
    palette.setColor(QPalette::ButtonText,ColorMessage::color_list_[index][0]);
    palette.setColor(QPalette::Button,ColorMessage::color_list_[index][1]);
    ui_->countdown_message_comboBox->setPalette(palette);
    ui_->countdown_message_comboBox->setStyleSheet("color: "+ColorMessage::color_name_list_[index][0] +
                "; background-color: "+ColorMessage::color_name_list_[index][1]);
  }
  ui_->countdown_message_comboBox->setCurrentIndex(i);
}

void MainWindow::updateNetworkUI()
{
  //stopDisplays();
  status_icon_->clear();
  status_label_->clear();
  clients_label_->clear();
  reload_client_socket_pushbutton_->disconnect();
  reload_client_socket_pushbutton_->hide();

  timer_elite_.stop();
  timer_elite_.disconnect();

  if(network_.network_option()== Network::Mode::Server)
  {
    status_label_->setText("Mode Serveur");
    updateEnabledScreensWidget(qApp->screens().size(),countdown_widget_.options().enabled_screens);
    ui_->network_commands_textEdit->show();
    if(network_.serverIsListening())
    {
      status_icon_->setPixmap( QPixmap(":/images/time.png"));
      if(network_.serverClientsConnected()>0)
      {
          QList<int> tmp;
          tmp.append(0);
          tmp.append(1);
          tmp.append(2);
          tmp.append(3);
          tmp.append(4);
          tmp.append(5);
        network_.serverCommandUpdateEnabledScreens(qApp->screens().size(),
                                                   tmp);
                                         //countdown_widget_.options().enabled_screens);
        status_icon_->setPixmap( QPixmap(":/images/ok.png"));
        if(network_.serverClientsConnected()>1)
          clients_label_->setText(QString::number(network_.serverClientsConnected()) + " clients connectés");
        else
          clients_label_->setText(QString::number(network_.serverClientsConnected()) + " client connecté");
      }
    }
    else
      status_icon_->setPixmap( QPixmap(":/images/error.png"));
  }
  else if(network_.network_option()== Network::Mode::Client)
  {
    status_label_->setText("Mode Client");
    reload_client_socket_pushbutton_->show();
    connect(reload_client_socket_pushbutton_,SIGNAL(clicked()),&network_,SLOT(updateNetwork()));
    ui_->network_commands_textEdit->hide();
    if(network_.clientSocketState() == QAbstractSocket::ConnectedState)
    {
      status_icon_->setPixmap( QPixmap(":/images/ok.png"));
      clients_label_->setText("Connecté");
      disconnect(reload_client_socket_pushbutton_);
      reload_client_socket_pushbutton_->hide();
    }
    else
    {
      status_icon_->setPixmap( QPixmap(":/images/error.png"));
      clients_label_->setText("Non connecté");
      updateEnabledScreensWidget(qApp->screens().size(),countdown_widget_.options().enabled_screens);
      reload_client_socket_pushbutton_->show();
    }
  }
  else
  {
    status_label_->setText("Mode local");
    updateEnabledScreensWidget(qApp->screens().size(),countdown_widget_.options().enabled_screens);
    ui_->network_commands_textEdit->hide();
  }

  if(network_.connect_to_elite())
  {
    elite_broadcast_widget_.show();
    elite_pilots_positions_widget_.show();
    remote_display_widget_.show();
    elite_broadcast_widget_.setFixedWidth(400);
    elite_pilots_positions_widget_.setFixedWidth(350);
    remote_display_widget_.setFixedWidth(400);
    elite_broadcast_widget_.disconnect();
    connect(&network_,SIGNAL(raceInformationDataReceived(const QString &)),
            &elite_broadcast_widget_,SLOT(raceInformationDataReceived(const QString &)));
    //connect(&timer_elite_,SIGNAL(timeout()),&network_,SLOT(getEliteClassification()));
    //timer_elite_.start(5000);
    reload_elite_connexion_pushbutton_->show();
    remote_display_widget_.disconnect();
    connect(&remote_display_widget_,SIGNAL(send_remoteDisplay_message(const QString &)),
            &network_,SLOT(sendRemoteDisplayMessage(const QString &)));

    if ( (network_.eliteSocketState() == QAbstractSocket::ConnectedState) &&
        (network_.remoteDisplaySocketState() == QAbstractSocket::ConnectedState))//or
        //(network_.eliteSocketState() == QAbstractSocket::ConnectingState))
    {
      elite_connexion_label_->setText("Connecté à Elite");
      reload_elite_connexion_pushbutton_->hide();
      reload_elite_connexion_pushbutton_->disconnect();
      elite_broadcast_widget_.show();
      elite_broadcast_widget_.reset();
      elite_pilots_positions_widget_.show();
      elite_pilots_positions_widget_.reset();
      remote_display_widget_.show();
    }
    else
    {
      elite_connexion_label_->setText("Non Connecté à Elite");
      elite_broadcast_widget_.reset();
      elite_pilots_positions_widget_.reset();
      reload_elite_connexion_pushbutton_->show();
      connect(reload_elite_connexion_pushbutton_,SIGNAL(clicked()),
              &network_,SLOT(updateEliteNetwork()));
      elite_broadcast_widget_.hide();
      elite_pilots_positions_widget_.hide();
      remote_display_widget_.hide();
    }
  }
  else
  {
    elite_broadcast_widget_.hide();
    elite_pilots_positions_widget_.hide();
    remote_display_widget_.hide();
    elite_broadcast_widget_.disconnect();
    elite_pilots_positions_widget_.disconnect();
    remote_display_widget_.disconnect();
    reload_elite_connexion_pushbutton_->hide();
    ui_->centralWidget->updateGeometry();
  }
}

void MainWindow::sendTcpUpdateEnabledScreens()
{
  QList<int> tmp;
  tmp.append(0);
  tmp.append(1);
  tmp.append(2);
  tmp.append(3);
  tmp.append(4);
  tmp.append(5);
  network_.serverCommandUpdateEnabledScreens(qApp->screens().size(),
                                             tmp);
                                     //countdown_widget_.options().enabled_screens);
}

void MainWindow::updateNetworkCommandLabel(const QString & adress,
                                           const QString & message)
{
  QString str;
  str = QTime::currentTime().toString() + " - ";
  str += "(" + adress + ") - ";
  str += message;
  network_command_history_.append(str);
  ui_->network_commands_textEdit->insertPlainText("\n"+ str);
  ui_->network_commands_textEdit->verticalScrollBar()->setValue(
        ui_->network_commands_textEdit->verticalScrollBar()->maximum());
}

void MainWindow::displayWarningBox(const QString & str)
{
  stopDisplays();
  QMessageBox::warning(this, "Erreur", str);
}


void MainWindow::on_enabled_all_screens_pushButton__clicked()
{
  const QSignalBlocker blocker(this);
  for(QCheckBox* x : enabled_screens_checkBox_list_)
      x->setChecked(true);
}
