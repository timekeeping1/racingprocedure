#ifndef COLORMESSAGE_H
#define COLORMESSAGE_H

#include <QObject>
#include <QColor>

class ColorMessage
{
public:
  ColorMessage(const QString&, const int);

  const QString message();
  void set_message(const QString&);
  int color_model_index();
  void set_color_model_index(const int);
  const QColor background_color();
  const QColor font_color();
  const QString background_color_name();
  const QString font_color_name();
  const QString toSettings();

private:
  QString message_;
  int color_model_index_;

public:
  static QList<QList<QColor>> color_list_;
  static QList<QList<QString>> color_name_list_;
  static ColorMessage fromSettings(const QString&);
  static QList<ColorMessage> listFromSettings(const QStringList&);
  static QString toSettings( ColorMessage&);
  static QStringList toSettings( QList<ColorMessage>&);
  static QStringList message_list( QList<ColorMessage>&);
  static int colorsToIndex(const QColor&, const QColor&);
};

#endif // COLORMESSAGE_H
